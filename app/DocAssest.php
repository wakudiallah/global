<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class DocAssest extends Model
{
    //`id`, `file`, `status`, `user_id`, `deleted_at`, `created_at`, `updated_at`
    
    protected $table = 'doc_assest';

    protected $fillable = ['file', 'message', 'status', 'user_id'];

    use SoftDeletes;

	protected $guarded = ["id"]; 
	protected $dates   = ['deleted_at'];
	public $timestamps = true;

}
