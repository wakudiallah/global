<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class Remark extends Model
{
    //`id`, `id_remark`, `desc`, `color`, `deleted_at`, `created_at`, `updated_at`

	protected $table = 'remark';

    protected $fillable = ['id_remark','desc', 'color', 'role_id' ];

    use SoftDeletes;

	protected $guarded = ["id"]; 
	protected $dates   = ['deleted_at'];
	public $timestamps = true;

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function role()
    {
        return $this->belongsTo(Role::class);
    }

    public function workgroup() {
        return $this->belongsTo('App\Role','role_id','id'); 
    }


}
