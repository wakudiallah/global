<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class DocCust extends Model
{
    protected $table = 'doc_cust';

    protected $fillable = ['cus_id', 'doc_pdf', 'name', 'type','user_id', 'verification' ];


	protected $guarded = ["id"]; 
	public $timestamps = true;

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function doc()
    {
        return $this->hasMany(DocCust::class);
    }

    public function pdf()
    {
        return $this->belongsTo('App\praaplication', 'id_cus', 'cus_id');
    }

    public function spekar()
    {
        return $this->belongsTo('App\praapplication', 'cus_id', 'id_cus');
    }

    
}
