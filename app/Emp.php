<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class Emp extends Model
{
    protected $table = 'emp';

    protected $fillable = ['Emp_Code','Emp_Desc', 'Reg_Dt', 'Act' ];

    use SoftDeletes;

	protected $guarded = ["id"]; 
	protected $dates   = ['deleted_at'];
	public $timestamps = true;

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function emp()
    {
        return $this->hasMany(praapplication::class);
    }
}
