<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class praapplication extends Model
{
    
    protected $table = 'praapplication';

    protected $fillable = ['name','id_cus','ic','old_ic','notelp','employment_code','emp_code','elaun', 'pot_bul', 'loanpkg_code', 'gaji_asas', 'jml_pem', 'act', 'status', 'routeto', 'stage', 'user_id', 'emp_type', 'process6' ];

    use SoftDeletes;

	protected $guarded = ["id"]; 
	protected $dates   = ['deleted_at'];
	public $timestamps = true;

    
    public function spekar()
    {
        return $this->belongsTo('App\DocCust', 'id_cus', 'cus_id');
    }

    public function stages()
    {
        return $this->belongsTo('App\Stage', 'stage', 'id_stage');
    }
	
    public function majikan()
    {
        return $this->belongsTo('App\Emp', 'emp_code', 'id');
    }

    
    public function remarks()
    {
        return $this->belongsTo('App\Remark', 'status', 'id');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function print()
    {
        return $this->belongsTo('App\User', 'process5', 'id');
    }

    public function loanpkg()
    {
        return $this->belongsTo('App\Loanpkg', 'loanpkg_code', 'id');
    }    

    public function doc()
    {
        return $this->belongsTo('App\DocCust', 'id_cus', 'cus_id');
    }

    public function doc1()
    {
        return $this->belongsTo('App\DocCust', 'id_cus', 'cus_id');
    }

     public function remark_6()
    {
        return $this->belongsTo('App\History', 'id_cus', 'cus_id');
    }

    public function historydate()
    {
        return $this->belongsTo('App\History', 'id_cus', 'cus_id');
    }

    public function historydate2() {
        return $this->belongsTo('App\History', 'id_cus','cus_id')->Where('remark_id','=', 'stage');
    }

    public function MO()
    {
        return $this->belongsTo('App\User', 'assessment', 'id');
    }

}
