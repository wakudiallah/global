<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class Loanpkg extends Model
{
    protected $table = 'loanpkg';

    protected $fillable = ['LnPkg_Code','Ln_Desc','Min_Ten','Max_Ten','Mln_amt','Max_amt','Min_Service','Min_Age','Max_Age','Ln_Int','Act'];

    use SoftDeletes;

	protected $guarded = ["id"]; 
	protected $dates   = ['deleted_at'];
	public $timestamps = true;

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
