<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Model_has_role extends Model
{
    
	protected $table = 'model_has_roles';

    protected $fillable = ['role_id','model_id', 'model_type' ];

  
	public $timestamps = true;

    
    public function role() {
        return $this->belongsTo('App\Role','role_id','id'); // role_id(model) == id(role/workgroup)
    }

    public function user() {
        return $this->belongsTo('App\User','model_id','id'); //id =  table user == model_id = table model has roles
    }

}
