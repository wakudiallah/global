<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class MeetCust extends Model
{
    //`id`, `clarical`, `existing`, `date_disbustment`, `deleted_at`, `created_at`, `updated_at`
    
    protected $table = 'meet_cust';
    protected $fillable = [
       'cus_id','clarical', 'existing', 'date_disbustment'
    ];

    use SoftDeletes;

    protected $guarded = ["id"]; 
    protected $dates = ['deleted_at'];
    public $timestamps = true;
}
