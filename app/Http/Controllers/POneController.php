<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\praapplication;
use Illuminate\Support\Facades\Auth;
use App\Announcement;
use App\DocCust;
use DB;
use Mapper;
use DateTime;
use PDF;
use Hash;
use App\History;
use App\Remark;
use App\User;
use App\Role;
use App\Model_has_role;
use App\AddInfo;
use App\MeetCust;
use App\Package;
use App\Loanpkg;
use App\Employment;
use App\Emp;
use App\Homeimage;
use App\Tenure;
use App\Loan;
use App\LoanAmmount;
use Input;

class POneController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function uploadspekar_p1()
    {
        $user = Auth::user()->id;
        
        $spekar = praapplication::where('stage', 'W1' )->orderBy('created_at', 'desc')->get(); //Siap upload 

        return view('processor1.upload_spekar_new', compact('spekar'));
        
    }

    public function route_to_mo()
    {
        $user = Auth::user()->id;
        
        $spekar = praapplication::where('stage', 'W1' )->where('routeto', $user)->where('spekar', '!=', 'NULL')->orderBy('created_at', 'desc')->get(); //Siap upload SPEKAR  //W2 Ready upload Spekar

        return view('processor1.pending_route_to_mo', compact('spekar'));
        
    }

    
    public function iclocreject(Request $request, $id)
    {
        
        $user = Auth::user()->id;

        $process2 = $request->input('process2');
        $note     = $request->input('note');

       praapplication::where('id_cus', $id)->update(array('stage' => 'W14', 'routeto' => $process2  )); 

       $request                  = new History;

        $request->cus_id          = $id;  
        $request->activity        = "1";
        $request->remark_id       = 'W14';
        $request->user_id         = $user;
        $request->note            = $note;
        $request->save();

        return redirect('assessment1')->with(['update' => 'Data rejected successfully']);
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
