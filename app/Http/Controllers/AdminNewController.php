<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\praapplication;
use Illuminate\Support\Facades\Auth;
use App\Announcement;
use App\Emp;
use App\DocCust;
use DB;
use Mapper;
use DateTime;
use PDF;
use Hash;
use App\History;
use App\Remark;
use App\User;
use App\Role;
use App\Model_has_role;
use App\DocAssest;
class AdminNewController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user()->id;  
        
        //Processor3 Dashboard
        $tasklist_p3             = praapplication::orderBy('created_at', 'desc')->where('stage', 'W8')->get();
        $count_mbsb_processing_p3    = praapplication::where('stage', 'W8')->count();
        

        //MO DASHBOARD
        $count_submission    = praapplication::where('process2', $user)->where('stage', 'W0')->count();
        $count_doc_reject    = praapplication::where('process2', $user)->wherein('stage', ['W7','W13', 'W14'])->count();
        $count_pending_doc   = praapplication::where('process2', $user)->where('stage', 'W3')->count();
        $countmo_calculation = praapplication::where('process2', $user)->wherein('stage', ['W2', 'W6'])->count();
        $meetcus             = praapplication::where('process2', $user)->wherein('stage', ['W0','W1','W2','W3','W6','W7','W8','W9','W13', 'W14'])->orderBy('updated_at', 'desc')->get();
        


        //manager dashboard

        $countreject = praapplication::where('reject_mo', $user)->where('stage', 'W5')->orderBy('created_at', 'desc')->count();

        $countsub_manager       = praapplication::where('manager', $user)->where('stage', '!=' , 'W5')->count();

        

        $manager        = praapplication::where('reject_mo', $user)->where('stage', 'W5' )->orderBy('created_at', 'desc')->get();
        
        /*foreach($manager as $managers){

           if($managers->MO->manager == $user){

            $countmanager   = praapplication::where('stage', 'W5')->count();
            }
            else{
              $countmanager=0;
            }
        }*/


       

        $count          = praapplication::count();
        $countsub       = praapplication::where('submission', 0)->count();
        $praaplication  = praapplication::orderBy('created_at', 'desc')->get();
        $announc        =  Announcement::latest('id')->limit('1')->first();

        $role = Model_has_role::all();

        $users          = Auth::user();        
        $roless         = $users->model->role->id;

        $praaplication = praapplication::orderBy('created_at', 'desc')->get();


        //Processor 1
        
        $countp1newapp          = praapplication::where('routeto', $user)->where('stage', 'W14')->count();
        $countp1sendmbsb        = praapplication::where('stage', 'W1')->count();
        $countp1upload_spekar   = praapplication::where('stage', 'W1')->whereNULL('spekar')->count();
        $count_pen_mo           = praapplication::where('stage', 'W1')->where('spekar', '!=','')->count();
        $processor1             = praapplication::orderBy('created_at', 'desc')->where('stage', '=', 'W0')->get();
        $mbsb                   = DocAssest::orderBy('created_at', 'desc')->get();
        //$meetcus = praapplication::where('routeto', $user)->where('stage', '=', 'W2')->orWhere('stage', 'W3')->orWhere('stage', 'W6')->orderBy('created_at', 'desc')->get(); 

        //processor2
        $tasklist_p2          = praapplication::wherein('stage', ['W3','W8','W9','W10','W11','W4'])->orderBy('created_at', 'desc')->get();
        $mbsb_processing    = praapplication::where('stage', 'W4')->count();
        $pen_doc_check      = praapplication::where('stage', 'W9')->count();
        $pen_103            = praapplication::where('stage', 'W10')->count();

        $spekar = praapplication::where('stage', 'W0' )->orderBy('created_at', 'desc')->get(); //Siap upload SPEKAR  //W2 Ready upload Spekar

        /* Best majikan */
       /* $bestmajikan = praapplication::whereRaw('emp_code = (select max("emp_code") from praapplication)')->get(); 
        $bestmajikan = praapplication::select(DB::raw('MAX(emp_code) AS emp_code'))->limit('5')->get(); */

        //$bestmajikan = praapplication::Count('emp_code')->groupBy('emp_code')->take(5)->get();

        $dashp1 = praapplication::wherein('stage', ['W0', 'W1'])->orderBy('created_at', 'desc')->get();

        $countw0 = praapplication::where('stage', 'W0')->where('created_at', '>=', \Carbon\Carbon::now()->startOfMonth())->count();
        
        $bestmajikan = praapplication::groupBy('emp_code')->select('emp_code', DB::raw('count(emp_code) as total'))->take(5)->get();

        $bestagentmonth = praapplication::groupBy('user_id')->select('user_id', DB::raw('count(assessment) as total'))->take(5)->where('created_at', '>=', \Carbon\Carbon::now()->startOfMonth())->get();

        $bestagentyears = praapplication::groupBy('user_id')->select('user_id', DB::raw('count(user_id) as total'))->take(5)->where(DB::raw('YEAR(created_at)'), '=', date('Y'))->get(); 

        $assessment = praapplication::orderBy('created_at', 'desc')->where('stage', '=', 'W0')->get();
        //$mbsb       = DocAssest::orderBy('created_at', 'desc')->get();

        return view('adminpage.dashboard', compact('praaplication', 'count_submission','announc','count','bestmajikan','bestagentmonth','bestagentyears', 'countsub', 'countuser', 'role','roless', 'meetcus', 'spekar', 'tasklist_p2', 'countmanager', 'manager', 'assessment' ,'mbsb', 'dashp1', 'countw0', 'count_doc_reject', 'countp1sendmbsb', 'countp1upload_spekar', 'countmo_calculation','processor1','count_pen_mo','mbsb_processing','pen_doc_check','pen_103', 'count_pending_doc','user','userss', 'countreject', 'countsub_manager', 'count_mbsb_processing_p3', 'tasklist_p3'));
    }

    /**
     * view_location
     * Show the form for creating a new resource.
     
    @foreach($bestmajikan as $data)
        <li>
            {{$data->majikan->Emp_Desc}}
            <span class="pull-right"><b>{{$data->total}}</b> <small>Submittion</small></span>
        </li>
    @endforeach

     *
     * @return \Illuminate\Http\Response ver
     */
    
    public function ver($id)
    {
        $user = Auth::user();        
        $roless = $user->model->role->id;
        
        $remark = Remark::orderBy('role_id', 'asc')->where('role_id', '=', $roless)->get();
        $pra    =  praapplication::latest('id')->where('id_cus','=',$id)->first();
        

        return view('adminpage.praapplication.ver.index', compact('pra','remark'));
    }


    //Veryfikasi By Role
    public function submit_verify($id)
    {
        $user = Auth::user();        
        $roless = $user->model->role->id;

        $remark = Remark::orderBy('role_id', 'asc')->where('role_id', '=', $roless)->get();
        $reg = praapplication::latest('created_at')->where('id_cus',$id)->limit('1')->first();

        return view('adminpage.application.verifyapplication', compact('reg','remark'));
    }


    
    public function view_location($id)
    {
        $location =  praapplication::latest('id')->where('id','=',$id)->first();

        return view('adminpage.praapplication.location.index', compact('location'));
    }

     
    public function view_doc($cus_id)
    {
        $pra =  praapplication::latest('created_at')->where('id_cus','=',$cus_id)->limit('1')->first();
        $dcmt = DB::table('doc_cust')->select(DB::raw(" max(id) as id"))->where('cus_id',$cus_id)->groupBy('type')->pluck('id');
        $files = DocCust::whereIn('id', $dcmt)->get();

        $document1 = DocCust::latest('created_at')->where('cus_id',  $cus_id )->where('type',  '1' )->first();

        $document2 = DocCust::latest('created_at')->where('cus_id',  $cus_id )->where('type',  '2' )->first();

        $document3 = DocCust::latest('created_at')->where('cus_id',  $cus_id )->where('type',  '3' )->first();

        $document4 = DocCust::latest('created_at')->where('cus_id',  $cus_id )->where('type',  '4' )->first();
        
        $url = url('')."/documents/user_doc/".$pra->ic."/"; 

        if((!empty($document1->doc_pdf)) OR (!empty($document2->doc_pdf)) OR (!empty($document3->doc_pdf)) OR (!empty($document4->doc_pdf)) ) {
                # create new zip opbject
                $zip = new \ZipArchive();
                
                # create a temp file & open it
                $tmp_file = tempnam('.','');
                $zip->open($tmp_file, \ZipArchive::CREATE);
                
                # loop through each file
                foreach($files as $file){
                        $url2 = $url.$file->doc_pdf;
                        $url2 = str_replace(' ', '%20', $url2);
                        
                    
                           if (!function_exists('curl_init')){ 
                                die('CURL is not installed!');
                            }
                            $ch = curl_init();
                            curl_setopt($ch, CURLOPT_URL, $url2);
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                            $output = curl_exec($ch);
                            curl_close($ch);
                        $download_file = $output;
                                        
                        $type = substr($url2, -5, 5); 
                        #add it to the zip
                        $zip->addFromString(basename($url.$file->doc_pdf.'.'.$type),$download_file);
                }
                
                # close zip
                $zip->close();
        
        
                // Set Time Download
                date_default_timezone_set("Asia/Kuala_Lumpur");
                $time_name                       = date('Ymd');
                $time_download                   = date('Y-m-d H:i:s');
                # send the file to the browser as a download
                header('Content-disposition: attachment; filename=DOC-'.$time_name.'-'.$pra->ic.'.zip');
                header('Content-type: application/zip');
                readfile($tmp_file);
        }
    }

    public function submit_doc_cus($id)
    {
        $reg         = praapplication::latest('created_at')->where('id_cus',$id)->limit('1')->first();
         
        $ids = $reg->id_cus;
        $document1 = DocCust::latest('created_at')->where('cus_id',$id)->where('type','1')->first();

        return view('adminpage.application.index', compact('reg'));
    }
    
    public function uploads(Request $request,$id)
    {
        $cus_id      = $request->input('id_cus');
        $type        = $request->input('type');
        $verification = $request->input('verification');

        $reg         = praapplication::latest('created_at')->where('id_cus',$cus_id)->limit('1')->first();
         
        $ics = $reg->ic;
                  
        $ic_number2 =  str_replace('/', '', $ics);

                 
                  if ($request->hasFile('file'.$id)) {
                  $name = $request->input('document'.$id);
                 $file = $request->file('file'.$id);
                $tipe_file   = $_FILES['file'.$id]['type'];
                //if($tipe_file == "application/pdf") {


                 $filename = str_random(15).'-'.$name.'-'.$file->getClientOriginalName();
                 $destinationPath = 'documents/user_doc/'.$ic_number2.'/';
                 $file->move($destinationPath, $filename);
       

                $document               = new DocCust;
                $document->cus_id       = $cus_id;
                $document->doc_pdf      = $filename;
                $document->type         = $id;
                $document->verification = $verification;

                $document->save();
                 return response()->json(['file' => "$filename"]);

              // }       

                }  
    }


    
    

    
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function view_history($id)
    {
        $praapplication = praapplication::where('id_cus', $id)->first();
        $history = History::orderBy('id', 'ASC')->where('cus_id', $id)->get();

        return view('adminpage.praapplication.history.index', compact('history', 'praapplication'));
    }

    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
