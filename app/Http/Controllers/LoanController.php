<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\praapplication;
use Illuminate\Support\Facades\Auth;
use App\Announcement;
use App\Emp;
use App\DocCust;
use DB;
use Mapper;
use DateTime;
use PDF;
use Hash;
use App\History;
use App\Remark;
use App\User;
use App\Role;

class LoanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $users          = Auth::user();        
        $roless         = $users->model->role->id;
        if($roless=='3'){
             $praaplication             = praapplication::where('process2', $users->id)->orderBy('created_at', 'desc')->get();
        }
        else{
              $praaplication             = praapplication::orderBy('created_at', 'desc')->get();
        }
       
        //$praaplication = praapplication::orderBy('id', 'desc')->get();

        /*$praaplication = praapplication::join('history', 'praapplication.id_cus', '=', 'history.cus_id')
            ->where('praapplication.stage' )
            ->select('users.id', 'contacts.phone', 'orders.price')
            ->get(); */

        return view('adminpage.loan.index', compact('praaplication'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
