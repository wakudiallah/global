<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\praapplication;
use App\Authorizable;
use Illuminate\Support\Facades\Auth;
use App\Package;
use App\Loanpkg;
use App\Employment;
use App\Emp;
use Ramsey\Uuid\Uuid;
use App\DocCust;
use App\DocAssest;
use App\User;
use App\History;
use App\Sendmbsb;
use App\LoanDetail;
use App\TenureDetail;
use Mail;
use App\Model_has_role;
use Maatwebsite\Excel\Facades\Excel;
use DB;
use Carbon\Carbon;
use Disk;
use App\Loanammount;
use DateTime;
use Response;
use Session;

class PSecondController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    
    public function loan_internal_cal_perone($id)
    {
        $user = Auth::user()->id;

        $internal_cal = praapplication::where('stage', 'W4' )->where('id_cus', $id )->orderBy('created_at', 'desc')->get(); 

        return view('processor2.internal_calculation', compact('internal_cal'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function loan_cal($id)
    {
        $package    = Package::all();
        $employment = Employment::all();
        $loanpkg    = Loanpkg::all();
        $emp        = Emp::all();
        $pra =      Praapplication::where('id_cus', $id)->first(); 

        
        return view('processor2.loan_calculation_p2', compact('package', 'employment', 'loanpkg', 'emp', 'pra'));
    }

    public function post_loan_cal_p2(Request $request, $id)
    {
        $user = Auth::user()->id;
        $id_pra = $request->input('id_praapplication');

        $routeback = $request->input('process5');
        
        $stagep6     = $request->input('remarkp6');

        $notep6      = $request->input('notep6');

        if($request->input('remarkp6') == 'W6' ){  //back mo

            $id_pra = $request->input('id_praapplication');

            praapplication::where('id_cus', $id)->update(array('routeto' => $routeback, 'stage' => $request->input('remarkp6'), 'process6' =>  $user, 'remark6' =>  $stagep6));
        
            $request                  = new History;

            $request->cus_id          = $id;  
            $request->activity        = "";
            $request->remark_id       = $stagep6;
            $request->user_id         = $user;
            $request->note            = $notep6;
            $request->save();

            return redirect('adminnew')->with(['update' => 'Data saved successfully']);
        }

        elseif($request->input('remarkp6') == 'W9'){ //next
            
            $id_pra = $request->input('id_praapplication');

            praapplication::where('id_cus', $id_pra)->update(array( 'stage' => $stagep6, 'process6' =>  $user, 'remark6' =>  $stagep6));
        
            $request                  = new History;

            $request->cus_id          = $id;  
            $request->activity        = "";
            $request->remark_id       = $stagep6;
            $request->user_id         = $user;
            $request->note            = "";
            $request->save();
        }

        return redirect('loan-eli/'.$id_pra.'/step2')->with(['update' => 'Data saved successfully']);

    }
    
    

    public function post_tenus_p2(Request $request)
    { 
        
        $id = $request->input('id_cus');
        $package    = Package::all();
        $employment = Employment::all();
        $loanpkg    = Loanpkg::all();
        $emp        = Emp::all();
        $pra        = Praapplication::where('id_cus', $id)->first(); 

        $step1        = Praapplication::where('id_cus', $id)->first();
        $process6 = praapplication::where('stage', 'W4')->orWhere('stage', 'W9')->orWhere('stage', 'W10')->orWhere('stage', 'W8')->orderBy('created_at', 'desc')->get();


        $employment_code = $request->input('Employment');
        $gaji_asas = $request->input('gaji_asas');
        $elaun     = $request->input('elaun');
        $pot_bul   = $request->input('pot_bul');
        $jml_pem   = $request->input('jml_pem');


        $id_type      = $employment_code;
        $total_salary = $gaji_asas + $elaun ;
        $zbasicsalary = $gaji_asas + $elaun ;
        $zdeduction   = $pot_bul;

        $loan = LoanDetail::where('id_emp_type',$id_type)
                ->where('min_salary','<=',$total_salary)
                ->where('max_salary','>=',$total_salary)->limit('1')->get();

        //$id_loan  = $loan->id;  
        $id_loan  = $loan->first()->id;  
        
        $icnumber = $pra->first()->icnumber;
        $tanggal  = substr($icnumber,4, 2);
        $bulan    = substr($icnumber,2, 2);
        $tahun    = substr($icnumber,0, 2); 

        if($tahun > 30) {
            $tahun2 = "19".$tahun;
        }
        else {
             $tahun2 = "20".$tahun;
        }
       
        $lahir          = $tahun2.'-'.$bulan.'-'.$tanggal; 
        $lahir          =  date('Y-m-d', strtotime($lahir. ' - 16 month'));
        $oDateNow       = new DateTime();
        $oDateBirth     = new DateTime($lahir);
        $oDateIntervall = $oDateNow->diff($oDateBirth);

        $umur = 61 - $oDateIntervall->y;

        $tenure = TenureDetail::where('id_loan',$id_loan)->where('years','<', $umur)->where('years','!=','1')->get();

        return view('processor2.step1a', compact('package', 'employment', 'loan', 'emp', 'pra', 'loan', 'tenure', 'zbasicsalary', 'zdeduction', 'total_salary','id_loan', 'step1', 'process6'));

        
    
    }


    public function step1($id)
    {
        $user = Auth::user()->id;

        $step1 = praapplication::where('id_cus',$id)->first();
         //$financial = Financial::where('user_id',$id)->first();
         $loanammount = LoanAmmount::where('id_praapplication',$id)->first();
         $pra =      Praapplication::where('id_cus', $id)->first(); 
         $package    = Package::all();
        $employment = Employment::all();
        $loanpkg    = Loanpkg::all();
        $emp        = Emp::all();
        $process6 = praapplication::where('stage', 'W4')->orWhere('stage', 'W9')->orWhere('stage', 'W10')->orWhere('stage', 'W8')->where('routeto', $user)->orderBy('created_at', 'desc')->get();

        return view('processor2.step1', compact('step1', 'financial', 'loanammount', 'emp', 'pra', 'package', 'employment', 'loanpkg', 'emp', 'process6'));
    }


    public function store(Request $request)
    {
        //
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
