<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\praapplication;

class ReportManagerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function index()
    {
        
        $reportmanag = praapplication::where('stage', 'W5' )->orderBy('created_at', 'desc')->get();
        
        return view('adminpage.reporting.reportmanager.index', compact('reportmanag'));
    }

    public function make_new_cus($id)
    {
        
        $make_new_cus = praapplication::where('id_cus', $id)->where('stage', 'W5' )->orderBy('created_at', 'desc')->get();
        
        return view('adminpage.reporting.reportmanager.index', compact('make_new_cus'));
    }

    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function report_processor4()
    {
        $report = praapplication::where('stage', 'W12' )->orWhere('stage', 'W11')->orderBy('created_at', 'desc')->get();
        
        return view('adminpage.reporting.disbursement.index', compact('report'));
    }
    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
