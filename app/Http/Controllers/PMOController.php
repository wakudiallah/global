<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\praapplication;
use App\Authorizable;
use Illuminate\Support\Facades\Auth;
use App\Package;
use App\Loanpkg;
use App\Employment;
use App\Emp;
use Ramsey\Uuid\Uuid;
use App\DocCust;
use App\DocAssest;
use App\User;
use App\History;
use App\Sendmbsb;
use Mail;
use App\Model_has_role;
use Maatwebsite\Excel\Facades\Excel;
use DB;
use Carbon\Carbon;
use App\Loanammount;
use DateTime;
use Response;
use Session;
use Exception;
use PhpOffice\PhpWord\PhpWord;
use PDF;

class PMOController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user       = User::where('status', 1)->get();

        $workgroup  = Model_has_role::where('role_id', 3)->get();


        $employment = Employment::all();
        $emp        = Emp::all();
        $loanpkg    = Loanpkg::all();
        $assessment = praapplication::orderBy('created_at', 'desc')->get();

        return view('adminpage.customer.add_by_mo', compact('assessment', 'employment', 'emp', 'loanpkg', 'workgroup'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    
    
    public function mo_cal_check(Request $request)
    {
        $user = Auth::user()->id;

        $meetcus = praapplication::where('process2', $user)->wherein('stage', ['W2', 'W6'] )->orderBy('created_at', 'desc')->get(); 

        $workgroupprocessor2 = Model_has_role::where('role_id', 6)->get();

        return view('mo.loan_checks', compact('meetcus', 'workgroupprocessor2'));
        //C:\xampp\htdocs\global\global\resources\views\.blade.php
    }

    
    public function mo_cal_check_perone($id)
    {
        $user = Auth::user()->id;

        $meetcus = praapplication::where('process2', $user)->where('stage', 'W2')->where('id_cus', $id)->orderBy('created_at', 'desc')->get(); 

        $workgroupprocessor2 = Model_has_role::where('role_id', 6)->get();

        return view('mo.loan_checks', compact('meetcus', 'workgroupprocessor2'));
    }

    public function cal_change($id)
    {
        $user = Auth::user()->id;

        $meetcus = praapplication::where('process2', $user)->Where('stage', 'W6')->where('id_cus', $id)->orderBy('created_at', 'desc')->get(); 

        $workgroupprocessor2 = Model_has_role::where('role_id', 6)->get();

        return view('mo.loan_checks', compact('meetcus', 'workgroupprocessor2'));
    }

    public function doc_incomplete($id)
    {
        $user = Auth::user()->id;

        $docin = praapplication::where('process2', $user)->wherein('stage',['W7', 'W13'])->where('id_cus', $id)->orderBy('created_at', 'desc')->get(); 

        return view('mo.docincomplete', compact('docin'));            
    }
    
    public function doc_additional($id)
    {
        $user = Auth::user()->id;

        $docin = praapplication::where('process2', $user)->Where('stage', 'W13')->where('id_cus', $id)->orderBy('created_at', 'desc')->get(); 

        return view('mo.docincomplete', compact('docin'));  
    }

    public function pending_doc()
    {
        $user = Auth::user()->id;

        $meetcus = praapplication::where('process2', $user)->Where('stage', 'W3')->orderBy('created_at', 'desc')->get();
        $workgroupprocessor2 = Model_has_role::where('role_id', 6)->get(); 


        return view('mo.pending_doc', compact('meetcus', 'workgroupprocessor2'));  
    }


    public function pendingdoc_perone($id)  
    {
        $user = Auth::user()->id;

        $meetcus = praapplication::where('process2', $user)->Where('stage', 'W3')->Where('id_cus', $id)->orderBy('created_at', 'desc')->get();
        $loanammount = LoanAmmount::where('id_praapplication', $id)->limit('1')->first(); 

        $workgroupprocessor2 = Model_has_role::where('role_id', 6)->get(); 

        return view('mo.pending_doc_perone', compact('meetcus', 'workgroupprocessor2', 'loanammount'));  
    }

    public function doc_iclocreject($id)  
    {
        $user = Auth::user()->id;

        $meetcus = praapplication::where('process2', $user)->Where('stage', 'W14')->Where('id_cus', $id)->orderBy('created_at', 'desc')->get();

        $reg = praapplication::latest('id')->Where('id_cus', $id)->limit('1')->first();

        return view('mo.ic_loc_not_complete', compact('meetcus', 'workgroupprocessor2', 'reg'));

    }

    public function update_locic($id)  
    {
        $user = Auth::user()->id;

        praapplication::where('id_cus', $id)->update(array('stage' => 'W0' ));

        $request                  = new History;

        $request->cus_id          = $id;  
        $request->activity        = "1";
        $request->remark_id       = 'W0';
        $request->user_id         = $user;
        $request->note            = '';
        $request->save();

        return redirect('adminnew')->with(['update' => 'Data updated successfully']);
    }
    
    public function passreject_mo(Request $request,$id)
    {
        $user = Auth::user()->id;

        $stage           = $request->input('moremark');
        $manager           = $request->input('mng');
        $note            = $request->input('note');

        praapplication::where('id_cus', $id)->update(array('process5' => $user, 'stage' => $stage, 'reject_mo' =>  $manager ));


        $request                  = new History;

        $request->cus_id          = $id;  
        $request->activity        = "5";
        $request->remark_id       = $stage;
        $request->user_id         = $user;
        $request->note            = $note;
        $request->save();

        return redirect('pendingdoc')->with(['update' => 'Data saved successfully']);
    }


    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
