<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Illuminate\Database\Eloquent\SoftDeletes;

class Comm extends Model
{
    
	protected $table = 'comm';

    protected $fillable = ['LnPkg_Code','Dt_Start','Dt_Ent','Comm_MO','Comm_TeamLEad','Comm_Manager','Act'];

    use SoftDeletes;

	protected $guarded = ["id"]; 
	protected $dates   = ['deleted_at'];
	public $timestamps = true;

    public function user()
    {
        return $this->belongsTo(User::class);
    }   

}
