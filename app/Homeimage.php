<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class Homeimage extends Model
{
    
    protected $table = 'homeimage';

    protected $fillable = ['image','status'];

    use SoftDeletes;

    protected $guarded = ["id"]; 
    protected $dates = ['deleted_at'];
    public $timestamps = true;

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
