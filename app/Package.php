<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class Package extends Model
{
    protected $table = 'package';

    protected $fillable = ['name'];

    use SoftDeletes;

    protected $guarded = ["id"]; 
    protected $dates = ['deleted_at'];
    public $timestamps = true;

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
