<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    //return view('welcome');
    return view('utama.main');
});*/

Route::get('/test', function () {
    
    return view('adminpage.message.template_reply');
});

Route::get('/', 'Utama\UtamaController@index');
Route::post('praaplication/store', 'Praaplication\PraaplicationController@store');
Route::get('praapplication/{id}', 'Praaplication\PraaplicationController@kira');




Route::resource('cif', 'CifController');

Route::get('/hidden', 'Utama\UtamaController@hidden');

Route::get('about', 'Utama\UtamaController@about');
Route::get('service', 'Utama\UtamaController@service');
Route::get('faq', 'Utama\UtamaController@faq');
Route::get('contact', 'Utama\UtamaController@contact');
//Route::post('/praaplication/save', 'Praaplication\PraaplicationController@save_praaplication');

Route::resource('message', 'Utama\MessageController');
Route::get('/reply-que/{id}', 'Utama\MessageController@show');
Route::post('/reply_post', 'Utama\MessageController@update');

Route::resource('praaplication', 'Praaplication\PraaplicationController');

Route::post('/praaplication/kelayakan', 'Praaplication\PraaplicationController@kelayakan');
Route::get('/praaplication/upload-file/{id}', 'Praaplication\PraaplicationController@upload');
Route::post('/praaplication/upload-file/uploads/{id}', 'Praaplication\PraaplicationController@uploads');
Route::post('/praaplication/finish', 'Praaplication\PraaplicationController@finish');
Route::get('upload-praaplication', 'Praaplication\PraaplicationController@upload');

Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');

Route::get('/adminnew', 'AdminNewController@index')->name('adminnew');


Route::group( ['middleware' => ['auth']], function() {
    Route::resource('mo', 'MOController');
    Route::get('/add_leader/{id}', 'MOController@add_leader');
    Route::post('/mo/add_leader', 'MOController@post_leader'); 
    Route::get('/mo/update1/{id}', 'MOController@updatestatus1'); 
    Route::get('/mo/update2/{id}', 'MOController@updatestatus2');
    Route::get('/add_manager/{id}', 'MOController@add_manager');
    Route::post('/mo/add_manager', 'MOController@post_manager'); 
    

    Route::resource('users', 'UserController');
    Route::get('/users/edit/{id}', 'UserController@edit');
    Route::delete('/users/destroy/{id}', 'UserController@destroy');
    Route::resource('stage', 'StageController');
    Route::resource('profile1', 'ProfileController');
    Route::post('/save/profile/{id}', 'ProfileController@store_profile');
    Route::post('/save/background/{id}', 'ProfileController@store_background');
    Route::resource('cus_mo', 'CustomerMoController');
    
    Route::post('/profileupdate', 'ProfileController@update');
    

    Route::get('/userdata/update/{id}', 'UserController@updatestatus'); 
    Route::get('/userdata/update1/{id}', 'UserController@updatestatus1'); 
    Route::post('/users/status/{id}', 'UserController@status'); //tes
    
    Route::resource('roles', 'RoleController');
    Route::resource('loan', 'LoanController');
    
    Route::get('/location/show/{id}', 'AdminNewController@view_location');
    Route::get('/history/show/{id}', 'AdminNewController@view_history');
    Route::get('/doc/show/{cus_id}', 'AdminNewController@view_doc');
    Route::get('/doc/submit/{id}', 'AdminNewController@submit_doc_cus');
    Route::post('/doc/submit/uploads/{id}', 'AdminNewController@uploads');

    //Processor 1
    Route::get('/assess_addcus', 'AssessmentController@create');
    Route::get('/doc/spekar/{id}', 'AdminNewController@submit_verify');
    Route::get('/speakar/doc/{id}', 'AssessmentController@kira');
    Route::post('/oassessment/kira/uploads/{id}', 'AssessmentController@uploads');
    Route::post('/save/spekar/{id}', 'LoanEliController@store');
    Route::get('/doc/spekardown/{cus_id}', 'LoanEliController@show');
    Route::post('/routeto/user/{id}', 'LoanEliController@routeto');
    Route::post('/send_spekar', 'LoanEliController@spekar_routeto');
    Route::get('/uploadspekar_p1', 'POneController@uploadspekar_p1');
    Route::get('/route_to_mo', 'POneController@route_to_mo');
    Route::post('/iclocreject/{id}', 'POneController@iclocreject');

    

    //Processor 2 
    Route::get('/loan_internal/calculation/{id}', 'PSecondController@loan_internal_cal_perone');
    //Route::post('/save/process6/{id}', 'LoanEliController@process6'); //hidden hitungan dsr
    Route::post('/save/process6/{id}', 'PSecondController@post_loan_cal_p2');
    Route::get('/pdfbatchheader/{id}', 'LoanEliController@pdfbatchheader');
    Route::get('/processor2/loancal/{id}', 'PSecondController@loan_cal');
    Route::post('/p2/custtenus/result', 'PSecondController@post_tenus_p2');
    Route::get('/processor2/recount/{id}', 'PSecondController@loan_cal');
    Route::get('/processor2/{id}/step1', 'PSecondController@step1');
    //Route::post('/tenos/custtenus/store', 'LoanEliController@post_tenus_step1');
    //Route::post('/tenos/custtenus/store_step1', 'PSecondController@post_tenus_step1');
    
    
    
    
    //PRocess 3 
    Route::post('/save/process7/{id}', 'LoanEliController@process7');
    Route::post('/save/process8/{id}', 'LoanEliController@process8');

    //Processor 4
    Route::get('/loan-eli/process11', 'LoanEliController@process11');
    Route::get('/report_monthly', 'ReportManagerController@report_processor4');
    Route::get('/appdisbursement/{id}', 'LoanEliController@disbursement');
    Route::get('/rejectdisbursement/{id}', 'LoanEliController@rejectdisbursement');
    Route::post('/adddocprocess11/{id}', 'LoanEliController@adddocprocess11');
    
    
    //MO  
    Route::resource('/mo_cal', 'PMOController');
    Route::get('/mo_cal_check', 'PMOController@mo_cal_check');
    Route::get('/loan/eli/scoring/{id}', 'PMOController@mo_cal_check_perone');
    Route::get('/cal/change/{id}', 'PMOController@cal_change');
    Route::get('/doc/incomplete/{id}', 'PMOController@doc_incomplete');
    Route::get('/doc/additional/{id}', 'PMOController@doc_additional');
    Route::get('/pendingdoc', 'PMOController@pending_doc');
    Route::get('/pendingdoc/perone/{id}', 'PMOController@pendingdoc_perone');
    Route::get('/doc/icloc/{id}', 'PMOController@doc_iclocreject');    
    //Route::get('/doc/icloc/{id}', 'AssessmentController@kira');
    //Route::get('/assessment/kira/{id}', 'AssessmentController@kira');
    Route::get('/ic_loc_update/{id}', 'PMOController@update_locic');
    Route::post('/passreject/mo/{id}', 'PMOController@passreject_mo');
    
    //Route::post('/passreject/mo/{id}', 'LoanEliController@passreject_mo');
    Route::post('/download/spekar/{id}', 'LoanEliController@download_spekar');
    Route::post('/save/meetcus/{id}', 'LoanEliController@meetcus');
    Route::get('/tenos/custtenos/{id}', 'LoanEliController@kelayakantenos');
    
    Route::get('/upload_ic_loc_back/{id}', 'LoanEliController@upload_ic_loc_back');
    
    Route::post('/save_tenure', 'LoanEliController@save_tenure');
    Route::get('/resulttenus/{id}', 'LoanEliController@resulttenus');
    Route::post('/tenos/custtenus/store', 'LoanEliController@post_tenus_step1');
    Route::resource('/docincomplete', 'DocumentIncompleteController');
    Route::get('/nextcalchange', 'DocumentIncompleteController@nextcalchange');
    Route::get('/locic/rejectic_loc', 'DocumentIncompleteController@reject_ic_loc');

    //moaqs
    Route::resource('/moaqs', 'MoaqsController');
    Route::get('/moaqs/test', 'MoaqsController@show');
    
    

    //test verifikasi
    Route::get('/verifikasi/{id}', 'AdminNewController@ver');

    //Veryfy MO 1
    Route::post('/verifymo', 'AssessmentController@ver_mo');    
    
    Route::resource('/assess', 'AssessmentController');
    Route::resource('assessment', 'AssessmentController');
    Route::resource('assessment1', 'AssessmentController');
    Route::get('/assessment/kira/{id}', 'AssessmentController@kira');
    
    Route::post('/assessment/kira/uploads/{id}', 'AssessmentController@uploads');
    Route::get('/send/mbsb', 'AssessmentController@send_mbsb');
    Route::post('/send/mbsbdata', 'AssessmentController@post_send_mbsb');
    Route::get('/stage/update/{id}', 'AssessmentController@stage_update');

    //Send n attacth to gmail
    Route::get('/send/excel', 'AssessmentController@send_excel');
    Route::post('/send/excel', 'AssessmentController@send_excel');
    Route::post('/send_to_generate', 'AssessmentController@save_table');
    Route::post('/send_aset', 'AssessmentController@save_table');

    //Report Manager
    Route::resource('report-manager', 'ReportManagerController');
    Route::get('/make-new-cus/{id}', 'ReportManagerController@make_new_cus');
    

    Route::resource('posts', 'PostController');
    Route::resource('comments', 'CommentController');
    
    Route::resource('homeimage', 'Utama\HomeImageController');
    Route::get('testUrl/{id}', 'Utama\HomeImageController@edit');
    Route::get('/homeimage/update1/{id}', 'Utama\HomeImageController@update1');
    Route::get('/homeimage/update2/{id}', 'Utama\HomeImageController@update2');

    /*Setup Module*/
    Route::resource('package', 'PackageController');
    Route::resource('employment', 'EmploymentController');
    Route::resource('employment', 'EmploymentController');
    Route::resource('rate', 'RateController');
    Route::resource('loanpkg', 'LoanpkgController');
    Route::get('/loanpackage/update/{id}', 'LoanpkgController@act');
    Route::get('/loanpackage/update1/{id}', 'LoanpkgController@act1');
    Route::resource('comm', 'CommController');
    Route::resource('emp', 'EmpController');
    Route::get('/employer/update/{id}', 'EmpController@act');
    Route::get('/employer/update1/{id}', 'EmpController@act1');
    Route::resource('cal', 'CalController');
    Route::resource('announc', 'AnnounController');
    Route::resource('remark', 'RemarkController');

    Route::get('/announc/show/{id}', 'AnnounController@view_announc');
    Route::get('announc/show/attach/{id}', 'AnnounController@data');
    Route::get('announc/update1/{id}', 'AnnounController@update1');
    Route::get('announc/update2/{id}', 'AnnounController@update2');

    Route::post('/emp/show/{id}', 'EmpController@emp_show');

    Route::resource('muat', 'MuatController');

    Route::resource('loan-eli', 'LoanEliController');
    Route::get('/loan-eli/{user_id}/step1', 'LoanEliController@verify_step1');
    Route::get('/loan-eli/{user_id}/step2', 'LoanEliController@verify_step2');
    Route::get('/loan-eli/{user_id}/step3', 'LoanEliController@verify_step3');

Route::resource('geolocation', 'GeoLogController');
Route::get('geolocation/error/{id}', 'GeoLogController@error');
Route::get('geolocation/map/{lat}/{lng}/{location?}', 'GeoLogController@map');
Route::post('getLocation', 'GeoLogController@getlocation');



});
