<script src="https://code.jquery.com/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>


<div class="row clearfix">
    <!-- Task Info -->
    
        <div class="card">
            <div class="header bg-red">
                <h2>Loan Eligibility Checking and Calculation</h2>
            </div>
            <div class="body">
                <div class="table-responsive">
                    <table class="table table-hover dashboard-task-infos" id="example">
                        <thead>
                            <tr>
                                <th width="10%">#</th>
                                <th width="30%">Name</th>
                                <th width="20%">IC</th>
                                <th width="20%">Upload Spekar</th>
                                <th>Detail</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i = 1; ?>

                            @foreach($spekar as $data)
                            <tr>
                                <td>{{$i++}}</td>
                                <td>{{$data->name}}</td>
                                <td>{{$data->ic}}</td>
                                
                                <td>
                                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal{{$data->id}}"><i class="material-icons disabled">file_upload</i>
                                      Upload Spekar
                                    </button>
                                </td>
                                <td><button class="btn btn-success" aria-controls="collapse-{{$data->id}}" data-target="#collapseDetailOne{{$data->id}}" data-toggle="collapse" style="cursor:pointer;">Detail</button></td>    
                            </tr>
                            @endforeach
                            
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    
    <!-- #END# Task Info -->
    
</div>

@foreach($spekar as $datas)
<div id="collapseDetailOne{{$datas->id}}" class="collapse" aria-expanded="false" data-collapse-group="collapse-group" aria-labelledby="headingFive" data-parent="#accordionExample">
    <div class="row clearfix">
        <div class="card">
            <div class="header bg-red">
                <h2>Detail</h2>
            </div>
            <div class="body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-6">
                                <b>Name : </b>    
                            </div>
                            <div class="col-md-6">
                               <p>{{$datas->name}} </p>  
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <b>No Telf : </b>    
                            </div>
                            <div class="col-md-6">
                               <p>{{$datas->notelp}} </p>  
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <b>Employer : </b>    
                            </div>
                            <div class="col-md-6">
                               <p>{{$datas->majikan->Emp_Desc}} </p>  
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <b>By : </b>    
                            </div>
                            <div class="col-md-6">
                               <p style="color: red">{{$datas->user->name}} </p>  
                            </div>
                        </div>
                       
                    </div>
                    <div class="col-md-6">
                        
                        

                        <div class="row">
                            <div class="col-md-6">
                                <b>Basic Salary (RM) : </b>    
                            </div>
                            <div class="col-md-6">
                               <p>{{$datas->gaji_asas}} </p>  
                            </div>
                        </div>


                    </div>
                </div>
                    
                
            </div>
        </div>
    </div>
</div>
@endforeach


<!-- Modal -->
@foreach($spekar as $datax)
<div class="modal fade" id="exampleModal{{$datax->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><b>{{$datax->name}} <b> -- {{$datax->ic}}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        
        <!-- Spekar upload -->
        <div class="form-group" >
            <label for="probootstrap-date-arrival">Doc Spekar</label>
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="id_cus" value="{{ $datax->id_cus}}">
            <input type="hidden" name="type4" value="4" id="type4">

            <div class="probootstrap-date-wrap">
              <span class="icon ion-document"></span>
              <input id="fileupload4" class="form-control"  @if(empty($document4->name))  @endif   type="file" name="file4" >
            </div>

            <input type="hidden" name="document4"   id="documentx4"  value="Spekar">&nbsp; <span id="document4"> </span> 
              @if(!empty($document4->name))
              <span id="document4a"><a target='_blank' href="{{url('/')}}/documents/user_doc/{{$datax->ic}}/{{$document4->doc_pdf}}"> {{$document4->name}}</a><i class="glyphicon glyphicon-ok txt-color-green"></i></span> 
              @else
              <!--<a class='bnt btn-danger'>No Attachment Available</a>-->
              @endif
              
          </div>


      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>
@endforeach


@push('scripts')
<script type="text/javascript">
    $("[data-collapse-group]").on('show.bs.collapse', function () {
          var $this = $(this);
          var thisCollapseAttr = $this.attr('data-collapse-group');
          $("[data-collapse-group='" + thisCollapseAttr + "']").not($this).collapse('hide');
        });
</script>




<!-- upload file -->
<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
<script src="{{url('/')}}/assets/js/file/vendor/jquery.ui.widget.js"></script>

<script src="{{url('/')}}/assets/js/file/jquery.iframe-transport.js"></script>
<!-- The basic File Upload plugin -->
<script src="{{url('/')}}/assets/js/file/jquery.fileupload.js"></script>
<?php for ($x = 1; $x <= 12; $x++) {  ?>

<script type="text/javascript">
/*jslint unparam: true */
/*global window, $ */
$(function () {
    'use strict';

    // Change this to the location of your server-side upload handler:
    var url = window.location.hostname === 'blueimp.github.io' ?
                '//jquery-file-upload.appspot.com/' : '/assessment/kira/uploads/{{$x}}' 
    $('#fileupload{{$x}}').fileupload({
        url: url,
        dataType: 'json',
        success: function ( data) {
             var text = $('#documentx{{$x}}').val();
            $("#document{{$x}}").html("<a target='_blank' href='"+"{{url('/')}}/documents/user_doc/{{$datax->ic}}/"+data.file+"'>"+text+"</a><i class='glyphicon glyphicon-ok txt-color-green'></i>");
             $("#document{{$x}}a").hide();
             $("#a{{$x}}").val(data.file);
            $("#a{{$x}}").val(data.file);
      $("#a{{$x}}").val(data.file);

             document.getElementById("fileupload{{$x}}").required = false;
    
        }
       
    }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');

        
});
</script>
<?php } ?>
 
 @endpush