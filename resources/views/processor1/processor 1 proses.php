<!-- loaneli index -->
@extends('vadmin.tampilan')

@section('content')

    <section class="content">
        <div class="container-fluid">

            <div class="row clearfix"> <!-- Breadcrumber -->
                <div class="col-md-6">
                    <ol class="breadcrumb breadcrumb-col-pink">
                        <li><a href="javascript:void(0);"><i class="material-icons">layers</i> Loan Eligibility Checking and Calculation</a></li>
                    </ol>
                </div>
            </div> <!-- End of breadcrumber -->
                

            <div class="row clearfix">

                <!-- Visitors -->
                <!-- <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                    <div class="card">
                        <div class="body bg-pink">
                            <div class="sparkline" data-type="line" data-spot-Radius="4" data-highlight-Spot-Color="rgb(233, 30, 99)" data-highlight-Line-Color="#fff"
                                 data-min-Spot-Color="rgb(255,255,255)" data-max-Spot-Color="rgb(255,255,255)" data-spot-Color="rgb(255,255,255)"
                                 data-offset="90" data-width="100%" data-height="92px" data-line-Width="2" data-line-Color="rgba(255,255,255,0.7)"
                                 data-fill-Color="rgba(0, 188, 212, 0)">
                                12,10,9,6,5,6,10,5,7,5,12,13,7,12,11
                            </div>
                            <ul class="dashboard-stat-list">
                                <li>
                                    TODAY
                                    <span class="pull-right"><b>1 200</b> <small>USERS</small></span>
                                </li>
                                <li>
                                    YESTERDAY
                                    <span class="pull-right"><b>3 872</b> <small>USERS</small></span>
                                </li>
                                <li>
                                    LAST WEEK
                                    <span class="pull-right"><b>26 582</b> <small>USERS</small></span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div> -->
                <!-- #END# Visitors -->
            </div>

            <!-- ========================== Loan Check / sebelum meet customer MO ===================== -->
            @can('view_loan_checks')
            
            @endcan

            


            <!-- =========================== Upload Spekar / Processor 1 Status : Pending Spekar ========================= -->
            @can('view_upload_spekars')
                
                    @include('processor1.spekar_upload')

            @endcan 
            

            @can('view_loan_check_completes')
            <div class="row clearfix">
                <!-- Task Info -->
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="card">
                        <div class="header bg-red">
                            <h2>Loan Egibility Checking and Calculation</h2>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-hover dashboard-task-infos" id="example">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Name</th>
                                            <th>IC</th>
                                            <th>Date Received</th>
                                            <th>Remark</th>
                                            <th>Loan Egibility</th>
                                            <th>Doc</th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $i = 1; ?>

                                        @foreach($processor3 as $data)
                                        <tr>
                                            <td>{{$i++}}</td>
                                            <td>{{$data->name}}</td>
                                            <td>{{$data->ic}}</td>
                                            <td>{{$data->created_at->toFormattedDateString()}}</td>
                                            
                                            <td></td>
                                            <td>
                                              
                                                <a href="{{url('/doc/verify/'.$data->id_cus)}}" class="btn btn-danger"><i class="material-icons disabled">fingerprint</i> Loan Cal </a>
                                            </td>

                                            <td>
                                                @if((empty($data->doc->cus_id))) 
                                                <a href="{{url('/doc/submit/'.$data->id_cus)}}" class="btn bg-red"><i class="material-icons disabled">do_not_disturb_alt</i> </a>
                                                @else
                                                <a href="{{url('/doc/show/'.$data->id_cus)}}" class="btn bg-light-blue" target='_blank'> <i class="material-icons">library_books</i> </a>
                                                @endif
                                            </td>
                                            
                                               
                                        </tr>
                                        @endforeach
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- #END# Task Info -->
                
            </div>
            @endcan


        </div>

        <!-- Gak jadi dulu -->
        <!-- modal detail -->
        @include('shared._modaldetail')
        <!-- end of modal detail -->


    </section>

    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>

    <script>
        $(document).ready(function() {
        $('#example').DataTable();
        });
    </script>


@endsection