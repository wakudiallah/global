<!-- Notification -->
            <div class="row clearfix">
                @if ($message = Session::get('success')) 
                <div class="alert bg-green alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>{{ $message }}
                </div>
                @elseif($message = Session::get('delete'))
                <div class="alert bg-pink alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>{{ $message }}
                </div>
                @elseif($message = Session::get('update'))
                <div class="alert alert-warning alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>{{ $message }}
                </div>
                @endif
            </div>
            <!-- End of Notif -->