<!DOCTYPE html>
<html lang="en">
@include('layouts.main')
@yield('head')

<body>
<div id="wrapper">@yield('container')</div>
	@include('layouts.javascripts')
	@yield('javascripts')
</body>
</html>