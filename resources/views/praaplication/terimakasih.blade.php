@extends('layouts.main')

@section('content')
  <section class="probootstrap-cover overflow-hidden relative"  style="background-image: url('{{ asset('assets/images/kl.jpg') }}');" data-stellar-background-ratio="0.5"  id="section-home">
    <div class="overlay"></div>
    <div class="container">
      <div class="row align-items-center">
         
        <div class="col-md-12 probootstrap-animate">
        	
        	<div class="col-md">
				<h2 class="heading mb-2 display-4 font-light probootstrap-animate text-center">
				Terima kasih atas penyerahan anda, loan anda akan diproses tidak lama lagi</h2> 
				<p class="lead mb-5 probootstrap-animate">
				<!-- </p>
				<a href="onepage.html" role="button" class="btn btn-primary p-3 mr-3 pl-5 pr-5 text-uppercase d-lg-inline d-md-inline d-sm-block d-block mb-3">See OnePage Verion</a> 
				</p> -->
			</div>
        </div>
      </div>
    </div>
</section>

@endsection