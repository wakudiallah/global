<div class="col-sm-12">
    <h5>Message:</h5>
    <div class="form-group form-float @if ($errors->has('title')) has-error @endif">
        <div class="form-line">
            
           <p style="color: red !important">{{$message->message}}</p>
            
        </div>
    </div>
</div>

<input type="text" name="id" value="{{$message->id}}" class="hidden">

<div class="col-md-12">
    <h5>Reply :</h5>
    <div class="form-group @if ($errors->has('reply')) has-error @endif">
        {!! Form::textarea('reply', null, ['class' => 'form-control ckeditor', 'id' => 'reply']) !!}
        @if ($errors->has('reply')) <p class="help-block">{{ $errors->first('reply') }}</p> @endif
    </div>
</div>


@push('scripts')
<script src="//cdn.ckeditor.com/4.6.2/standard/ckeditor.js"></script>

<script type="text/javascript">  
    CKEDITOR.replace( 'reply', { 
    enterMode: CKEDITOR.ENTER_BR, 
    on: {'instanceReady': function (evt) { evt.editor.execCommand('');     }},
    });      
</script>
@endpush

