@extends('vadmin.tampilan')


@section('content')
	<section class="content">
        <div class="container-fluid">
            <div class="row clearfix"><!-- Breadcrumber -->
                <div class="col-md-6">
                    <ol class="breadcrumb breadcrumb-col-pink">
                        <li><a href="javascript:void(0);"><i class="material-icons">home</i> Dashboard</a></li>
                        <li class="active"><i class="material-icons">cloud_upload</i> upload</li>
                    </ol>
                </div>
            </div>
            <!-- End of breadcrumber -->

            <div class="row clearfix">
                <div class="card">
                    <div class="header bg-red">
                        <h2>Upload Submission</h2>
                    </div>
                    
                    <div class="body">
                        <div class="col-md">
                            <h5>Upload  :</h5>
                            <div class="form-group form-float @if ($errors->has('title')) has-error @endif">
                                    
                                {!! Form::file('title', null, ['class' => 'form-control']) !!}
                                @if ($errors->has('title')) <p class="help-block">{{ $errors->first('title') }}</p> @endif
                                    
                            </div>
                            <div class="row">
                                <div class="col-xs-6 col-sm-3 col-md-2 col-lg-2 col-md-offset-10">
                                    <button type="submit" class="btn bg-green btn-block btn-lg waves-effect">
                                        Submit
                                    </button>
                                    {{ csrf_field() }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- end of muat naik -->

            <div class="row clearfix">
                <div class="card">
                    <div class="header bg-red">
                        <h2>Submission on date</h2>
                    </div>
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-hover dashboard-task-infos" id="example">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>IC</th>
                                        <th>Date Received</th>
                                        <th>Date Submitted</th>
                                        <th>Process Level</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>#</td>
                                        <td>#</td>
                                        <td>#</td>
                                        <td>#</td>
                                        <td>#</td>
                                        <td>#</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>


    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>

    <script>
        $(document).ready(function() {
        $('#example').DataTable();
        });
    </script>
@endsection