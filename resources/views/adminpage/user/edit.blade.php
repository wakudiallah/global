@extends('vadmin.tampilan')

@section('content')
	<section class="content">
        <div class="container-fluid">

            <div class="row clearfix"> <!-- Breadcrumber -->
                <div class="col-md-6">
                    <ol class="breadcrumb breadcrumb-col-pink">
                        <li><a href="{{url('users')}}"><i class="material-icons">person</i> User</a></li>
                        <li class="active"><i class="material-icons">person_add</i> Edit</li>
                    </ol>
                </div>
            </div> <!-- End of breadcrumber -->

		    <div class="row clearfix">
		        <div class="col-lg-12">
		        	<div class="card">
		        		<div class="header bg-red">Edit User</div>
		        		<div class="body">
				            {!! Form::model($user, ['method' => 'PUT', 'route' => ['users.update',  $user->id ] ]) !!}
		                            
									<!-- Name Form Input -->
									<div class="form-group form-float @if ($errors->has('name')) has-error @endif">
									    <div class="form-line">
									        {!! Form::label('name', 'Name') !!}
									        {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Name']) !!}
									    </div>
									    @if ($errors->has('name')) <p class="help-block">{{ $errors->first('name') }}</p> @endif
									</div>

									<!-- email Form Input -->
									<div class="form-group form-float @if ($errors->has('email')) has-error @endif">
									    <div class="form-line">
									        {!! Form::label('email', 'Email') !!}
									        {!! Form::text('email', null, ['class' => 'form-control', 'placeholder' => 'Email']) !!}
									    </div>
									    @if ($errors->has('email')) <p class="help-block">{{ $errors->first('email') }}</p> @endif
									</div>

									<!-- password Form Input -->
									<div class="form-group form-float @if ($errors->has('password')) has-error @endif">
									    <div class="form-line">
									        {!! Form::label('password', 'Password') !!}
									        {!! Form::password('password', ['class' => 'form-control', 'placeholder' => 'Password']) !!}
									    </div>
									    @if ($errors->has('password')) <p class="help-block">{{ $errors->first('password') }}</p> @endif
									</div>

									<!-- Roles Form Input -->
									<div class="form-group form-float @if ($errors->has('roles')) has-error @endif">
									    <div class="form-line">
									        {!! Form::label('roles', 'Workgroup') !!}
									        {!! Form::select('roles', $roles, isset($user) ? $user->roles->pluck('id')->toArray() : null,  ['class' => 'form-control']) !!}
									    </div>
									    @if ($errors->has('roles')) <p class="help-block">{{ $errors->first('roles') }}</p> @endif
									</div>

									<div class="form-group form-float @if ($errors->has('status')) has-error @endif">
									    <div class="form-line">
									        {!! Form::label('status','Status') !!}
									        {!! Form::select('status',[1=>'Active',0=>'Inactive'],null,['class'=>'form-control']) !!}
									    </div>
									    @if ($errors->has('status')) <p class="help-block">{{ $errors->first('status') }}</p> @endif
									</div>


									<div class="row demo-button-sizes">
										<div class="col-md-11"></div>
										<div class="col-md-1">
											<!-- Submit Form Button -->
		                            		{!! Form::submit('Save', ['class' => 'btn btn-lg btn-success']) !!}
										</div>
									</div>
		                            
		                        {!! Form::close() !!}
			            </div>
		            </div>
		        </div>
		    </div>
        </div>
    </section>
@endsection