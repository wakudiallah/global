@extends('vadmin.tampilan')

@section('content')

	<section class="content">
        <div class="container-fluid">

            <div class="row clearfix"> <!-- Breadcrumber -->
                <div class="col-md-6">
                    <ol class="breadcrumb breadcrumb-col-pink">
                        <li><a href="{{route('mo.index')}}"><i class="material-icons">person</i> User</a></li>
                        <li class="active"><i class="material-icons">person_add</i>MO User</li>
                    </ol>
                </div>
            </div> <!-- End of breadcrumber -->

		    <div class="row clearfix">
		        <div class="col-lg-12">
		        	<div class="card">
		        		<div class="header bg-red">Add</div>
		        		<div class="body">
				            
                            {!! Form::model($mo, ['method' => 'PUT', 'route' => ['mo.update', $mo->id] ]) !!}

				            	@include('adminpage.user.mo.form')
				                
				                <!-- Submit Form Button -->
				                <div class="row clearfix">
                                	<div class="col-xs-6 col-sm-3 col-md-2 col-lg-2 col-md-offset-10">
                                        <button type="submit" class="btn bg-green btn-block btn-lg waves-effect">
                                            Save
                                        </button>
                                        {{ csrf_field() }}
                                    </div>
                                </div>
				            {!! Form::close() !!}
			            </div>
		            </div>
		        </div>
		    </div>
        </div>
    </section>

@endsection