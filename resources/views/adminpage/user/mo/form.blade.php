

<!-- Name Form Input -->
<div class="form-group form-float @if ($errors->has('name')) has-error @endif">
    <div class="form-line">
        {!! Form::label('name', 'Name') !!}
        {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Name']) !!}
    </div>
    @if ($errors->has('name')) <p class="help-block">{{ $errors->first('name') }}</p> @endif
</div>

<!-- email Form Input -->
<div class="form-group form-float @if ($errors->has('email')) has-error @endif">
    <div class="form-line">
        {!! Form::label('email', 'Email') !!}
        {!! Form::text('email', null, ['class' => 'form-control', 'placeholder' => 'Email']) !!}
    </div>
    @if ($errors->has('email')) <p class="help-block">{{ $errors->first('email') }}</p> @endif
</div>

<!-- password Form Input -->
<div class="form-group form-float @if ($errors->has('password')) has-error @endif">
    <div class="form-line">
        {!! Form::label('password', 'Password') !!}
        {!! Form::password('password', ['class' => 'form-control', 'placeholder' => 'Password']) !!}
    </div>
    @if ($errors->has('password')) <p class="help-block">{{ $errors->first('password') }}</p> @endif
</div>

<!-- Roles Form Input -->

<input type="text" name="roles" value="3" class="hidden">


<!-- <div class="form-group form-float @if ($errors->has('status_head')) has-error @endif">
    <div class="form-line">

        {!! Form::label('head','Leader / Follower') !!}
        {!! Form::select('status_head',[1=>'Leader',0=>'Follower'],null,['class'=>'form-control']) !!}
        
    </div>
    @if ($errors->has('status_head')) <p class="help-block">{{ $errors->first('status_head') }}</p> @endif
</div> -->

<div class="form-group form-float @if ($errors->has('status')) has-error @endif">
    <div class="form-line">
        {!! Form::label('status','Status') !!}
        {!! Form::select('status',[1=>'Active',0=>'Non Active'],null,['class'=>'form-control']) !!}
    </div>
    @if ($errors->has('status')) <p class="help-block">{{ $errors->first('status') }}</p> @endif
</div>


<!-- <input type="text" name="status" value="1" class="hidden"> -->

<!-- Permissions -->
@if(isset($user))
    @include('shared._permissions', ['closed' => 'true', 'model' => $user ])
@endif