@extends('vadmin.tampilan')

@section('content')

    <section class="content">
        <div class="container-fluid">

            <div class="row clearfix"> <!-- Breadcrumber -->
                <div class="col-md-6">
                    <ol class="breadcrumb breadcrumb-col-pink">
                        <li><a href="javascript:void(0);"><i class="material-icons">home</i> Dashboard</a></li>
                    </ol>
                </div>
            </div> <!-- End of breadcrumber -->

            <!-- Running Text -->
                <div class="info-box-3 bg-red hover-zoom-effect">
                    <div class="icon">
                        <i class="material-icons">email</i>
                    </div>
                    <div class="content">
                        <div class="headline-text">
                            @if(empty($announc->id))
                                <marquee scrollamount="5" width="1000px" onmouseover="stop();" onmouseout="start();" ></marquee>
                            @else
                                <marquee scrollamount="5" width="1000px" onmouseover="stop();" onmouseout="start();" ><a href="{{url('/announc/show/'.$announc->id)}}" style="color:white; font-size: 20px; line-height: 60px !important; text-decoration: none;" onMouseOver="this.style.color='#a39999'" onMouseOut="this.style.color='#FFF'" > {{$announc->title}} </a></marquee>
                            @endif
                        </div>
                    </div>
                    
                </div> <!-- End of running text -->



                
        <!-- =====================  Task Admin  ================== -->
            
            @if( $roless == '1') 
            <div class="row clearfix">
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box bg-pink hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">forum</i>
                        </div>

                        <div class="content">
                            <div class="text">Assessment</div>
                            <div class="number count-to" data-from="0" data-to="{{ $count_submission }}" data-speed="15" data-fresh-interval="20"></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box bg-cyan hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">person_add</i>
                        </div>
                        <div class="content">
                            <div class="text">Submission</div>
                            <div class="number count-to" data-from="0" data-to="{{ $countsub }}" data-speed="1000" data-fresh-interval="20"></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box bg-light-green hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">playlist_add_check</i>
                        </div>
                        <div class="content">
                            <div class="text">Disbursement</div>
                            <div class="number count-to" data-from="0" data-to="0" data-speed="1000" data-fresh-interval="20"></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box bg-orange hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">help</i>
                        </div>
                        <div class="content">
                            <div class="text">Pending</div>
                            <div class="number count-to" data-from="0" data-to="0" data-speed="1000" data-fresh-interval="20"></div>
                        </div>
                    </div>
                </div>
            </div>  
        <!-- ===================== End Task Admin ==================== -->

            
        <!-- =====================  Task MO  ================== -->    
            @elseif($roless=='3')      
            <div class="row clearfix">
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box bg-pink hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">playlist_add_check</i>
                        </div>

                        <div class="content">
                            <div class="text">Submission</div>
                            <div class="number count-to" data-from="0" data-to="{{ $count_submission }}" data-speed="15" data-fresh-interval="20"></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" onclick="location.href='{{url('mo_cal_check')}}';">
                    <div class="info-box bg-cyan hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">work</i>
                        </div>
                        <div class="content">
                            <div class="text">Loan Eligibility & Scoring</div>
                            <div class="number count-to" data-from="0" data-to="{{ $countmo_calculation }}" data-speed="1000" data-fresh-interval="20"></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" onclick="location.href='{{url('pendingdoc')}}';" >
                    <div class="info-box bg-lime hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">hourglass_empty</i>
                        </div>
                        <div class="content">
                            <div class="text">Pending Documentation </div>
                            <div class="number count-to" data-from="0" data-to={{$count_pending_doc}} data-speed="1000" data-fresh-interval="20"></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" onclick="location.href='{{route('docincomplete.index')}}';" >
                    <div class="info-box bg-orange hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">help</i>
                        </div>
                        <div class="content">
                            <div class="text">Rejected Doc </div>
                            <div class="number count-to" data-from="0" data-to={{$count_doc_reject}} data-speed="1000" data-fresh-interval="20"></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box bg-light-green hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">local_offer</i>
                        </div>
                        <div class="content">
                            <div class="text">Disbursement</div>
                            <div class="number count-to" data-from="0" data-to="0" data-speed="1000" data-fresh-interval="20"></div>
                        </div>
                    </div>
                </div>
                
            </div>


            <div class="row clearfix">
                <!-- Task Info -->
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="card">
                        <div class="header bg-red">
                            <h2>Task list</h2>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-hover dashboard-task-infos" id="example">
                                    <thead>
                                        <tr>
                                            <th class="hidden"></th>
                                            <th>#</th>
                                            <th>Name</th>
                                            <th>IC</th>
                                            <th>Phone</th>
                                            <th>Date Submit</th>
                                            <th>Status</th>
                                            <th>Activity</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        <?php $i = 1; ?>

                                        @foreach($meetcus as $data)

                                            <?php 
                                                $date_rev =  date('Y-m-d H:i:s ', strtotime($data->created_at));
                                                $today    =  date('Y-m-d H:i:s');
                                                $to_time = strtotime($today);
                                                $from_time = strtotime($date_rev);
                                                $di= ($to_time - $from_time) / (60*60);
                                            ?> 

                                        <tr>
                                            <td class="hidden">{{number_format($di,0,'.','.')}}</td>
                                            <td>{{$i++}}</td>
                                            <td>{{$data->name}}</td>
                                            <td>{{$data->ic}} </td>
                                            <td>{{$data->notelp}}</td>

                                            <td>
                                                {{$data->created_at->toDayDateTimeString()}} 
                                            </td>
                                            <td>
                                                <span class="label bg-{{$data->stages->color}}">{{$data->stages->desc}}</span>
                                            </td>
                                            
                                            
                                            <td class="text-center">
                                                <a href="{{url('/history/show/'.$data->id_cus)}}" class="btn bg-orange btn-circle waves-effect waves-circle waves-float" onclick="window.open('{{url('/history/show/'.$data->id_cus)}}', 'newwindow', 'width=750,height=400'); return false;"> <i class="material-icons">history</i> </a>
                                            </td>
                                            
                                            <td>
                                                @if($data->stage == 'W2')
                                                <a href="{{url('/loan/eli/scoring/'.$data->id_cus)}}" class="btn btn-sm btn-primary"><i class="material-icons">work</i>Eligibility Scoring  </a>
                                                @elseif($data->stage == 'W3')
                                                <a href="{{url('/pendingdoc/perone/'.$data->id_cus)}}" class="btn btn-sm bg-lime"><i class="material-icons">hourglass_empty</i>Pending Documentation</a>
                                                @elseif($data->stage == 'W6')
                                                <a href="{{url('/cal/change/'.$data->id_cus)}}" class="btn btn-sm btn-success"><i class="material-icons">sync_problem</i>Calculation & Change  </a>
                                                @elseif($data->stage == 'W7')
                                                <a href="{{url('/doc/incomplete/'.$data->id_cus)}}" class="btn btn-sm btn-warning"><i class="material-icons">description</i>Doc Incomplete  </a>
                                                @elseif($data->stage == 'W13')
                                                <a href="{{url('/doc/incomplete/'.$data->id_cus)}}" class="btn btn-sm btn-warning"><i class="material-icons">description</i>Additional Doc Req  </a>
                                                @elseif($data->stage == 'W14')
                                                <a href="{{url('/doc/icloc/'.$data->id_cus)}}" class="btn btn-sm btn-warning"><i class="material-icons">description</i>Doc LOC / IC </a>
                                                @endif
                                            </td>  
                                        </tr>
                                        @endforeach
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- #END# Task Info -->
            </div>  
        <!-- ===================== End Task MO ==================== -->


        <!-- =====================  Task Manager  ================== -->
            @elseif($roless=='5') <!-- Manager -->

            <div class="row clearfix">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" onclick="location.href='{{route('report-manager.index')}}';" style="cursor:pointer;" >
                    <div class="info-box bg-pink hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">close</i>
                        </div>

                        <div class="content">
                            <div class="text">Rejected</div>
                            <div class="number count-to" data-from="0" data-to="{{ $countreject }}" data-speed="15" data-fresh-interval="20"></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="info-box bg-cyan hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">person_add</i>
                        </div>
                        <div class="content">
                            <div class="text">Submission</div>
                            <div class="number count-to" data-from="0" data-to="{{ $countsub_manager }}" data-speed="1000" data-fresh-interval="20"></div>
                        </div>
                    </div>
                </div>
                
            </div>


            <div class="row clearfix">
                <!-- Task Info -->
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >
                    <div class="card">
                        <div class="header bg-red">
                            <h2>Task list</h2>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-hover dashboard-task-infos" id="example">
                                    <thead>
                                        <tr>
                                            <th class="hidden"></th>
                                            <th>#</th>
                                            <th>Name</th>
                                            <th>IC</th>
                                            <th>Date Submit</th>
                                            <th>Status</th>
                                            <th>Activity</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        <?php $i = 1; ?>

                                        @foreach($manager as $data)
                                            <?php 
                                                $date_rev =  date('Y-m-d H:i:s ', strtotime($data->created_at));
                                                $today    =  date('Y-m-d H:i:s');
                                                $to_time = strtotime($today);
                                                $from_time = strtotime($date_rev);
                                                $di= ($to_time - $from_time) / (60*60);
                                            ?> 

                                        <tr>
                                            <td class="hidden">{{number_format($di,0,'.','.')}}</td>
                                            <td>{{$i++}}</td>
                                            <td>{{$data->name}}</td>
                                            <td>{{$data->ic}}</td>

                                            <td>
                                                {{$data->created_at->toDayDateTimeString()}}
                                            </td>
                                            <td>
                                                <span class="label bg-{{$data->stages->color}}">{{$data->stages->desc}}</span>
                                            </td>
                                            
                                            
                                            <td class="text-center">
                                                <a href="{{url('/history/show/'.$data->id_cus)}}" class="btn bg-orange btn-circle waves-effect waves-circle waves-float" onclick="window.open('{{url('/history/show/'.$data->id_cus)}}', 'newwindow', 'width=750,height=400'); return false;"> <i class="material-icons">history</i> </a>
                                            </td>
                                            <td>
                                                @if($data->stage == 'W5')
                                                <a href="{{ url('make-new-cus/'.$data->id_cus) }}" class="btn btn-sm bg-pink"><i class="material-icons">border_color</i> Recommend new submission  </a>
                                                @endif
                                            </td>
                                               
                                        </tr>
                                        @endforeach
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- #END# Task Info -->                
            </div>
        <!-- =====================  End Task Manager  ================== -->


        <!-- ===================== Task Processor 1 ==================== -->
            @elseif($roless=='4') 

                <div class="row clearfix">
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12" onclick="location.href='{{route('assessment1.index')}}';" style="cursor:pointer; ">
                            <div class="info-box bg-red hover-expand-effect">
                                <div class="icon">
                                    <i class="material-icons">playlist_add_check</i>
                                </div>
                                
                                    <div class="content">
                                        <div class="text">Send to MBSB</div>
                                        <div class="number count-to" data-from="0" data-to="{{ $countw0 }}" data-speed="1000" data-fresh-interval="20"></div>
                                    </div>
                            </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12" onclick="location.href='{{url('uploadspekar_p1')}}';">
                        <div class="info-box bg-light-green hover-expand-effect">
                            <div class="icon">
                                <i class="material-icons">file_upload</i>
                            </div>
                            <div class="content">
                                <div class="text">Upload Spekar</div>
                                <div class="number count-to" data-from="0" data-to={{$countp1upload_spekar}} data-speed="1000" data-fresh-interval="20"></div>
                            </div>
                        </div>
                    </div> 
                     <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12" onclick="location.href='{{url('route_to_mo')}}';" style="cursor:pointer; ">
                        <div class="info-box bg-blue hover-expand-effect">
                            <div class="icon">
                                <i class="material-icons">exit_to_app</i>
                            </div>
                            <div class="content">
                                <div class="text">Route Mo</div>
                                <div class="number count-to" data-from="0" data-to="{{ $count_pen_mo }}" data-speed="1000" data-fresh-interval="20"></div>
                            </div>
                        </div>
                    </div> 
                </div>

            <div class="row clearfix">
                
                <div class="card">
                    <div class="header bg-red">
                        <h2>Task List </h2>
                    </div>
                    
                   <div class="body">
                                <!-- <input  value="Generate" class="btn btn-success" > -->
                                <table class="table" id="example2">
                                    <thead>
                                        <tr>
                                            
                                            <th>#</th>
                                            <th>Customer Name</th>
                                            <th>IC</th>
                                            <th>Phone</th>
                                            <th>Date Submit</th>
                                            <th>Status</th>
                                            <th>MO</th>
                                            <th>Activity</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $i = 1; ?>

                                        @foreach($dashp1 as $data)

                                            
                                        <tr>
                                            <td>{{$i++}}</td>
                                            <td>{{ $data->name }}</td>
                                            <td>{{ $data->ic }}</td>
                                            <td>{{ $data->notelp }}</td>
                                            <td>{{$data->created_at->toDayDateTimeString()}}</td>
                                            <td>
                                                @include('shared.stage')
                                            </td>
                                            <td>{{ $data->user->name }}</td>
                                            <td>
                                               <a href="{{url('/history/show/'.$data->id_cus)}}" class="btn bg-orange btn-circle waves-effect waves-circle waves-float" onclick="window.open('{{url('/history/show/'.$data->id_cus)}}', 'newwindow', 'width=750,height=400'); return false;"> <i class="material-icons">history</i> </a>
                                            </td>

                                        </tr>
                                    @endforeach
                                    

                                </form>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>  
        <!-- ===================== End Task Processor 1 ==================== -->

        <!-- =====================Task Processor2 ==================== -->
            
            @elseif($roless=='6')
                <div class="row clearfix">
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12" onclick="location.href='{{route('loan-eli.index')}}';" style="cursor:pointer; ">
                            <div class="info-box bg-pink hover-expand-effect">
                                <div class="icon">
                                    <i class="material-icons">border_color</i>
                                </div>
                                
                                    <div class="content">
                                        <div class="text">Loan Calculation</div>
                                        <div class="number count-to" data-from="0" data-to="{{ $mbsb_processing }}" data-speed="1000" data-fresh-interval="20"></div>
                                    </div>
                            </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12" onclick="location.href='{{route('loan-eli.index')}}';">
                        <div class="info-box bg-blue hover-expand-effect">
                            <div class="icon">
                                <i class="material-icons">rate_review</i>
                            </div>
                            <div class="content">
                                <div class="text">Doc Check</div>
                                <div class="number count-to" data-from="0" data-to={{$pen_doc_check}} data-speed="1000" data-fresh-interval="20"></div>
                            </div>
                        </div>
                    </div> 
                     <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12" onclick="location.href='{{route('loan-eli.index')}}';" style="cursor:pointer; ">
                        <div class="info-box bg-green hover-expand-effect">
                            <div class="icon">
                                <i class="material-icons">call</i>
                            </div>
                            <div class="content">
                                <div class="text">Call 103</div>
                                <div class="number count-to" data-from="0" data-to="{{ $pen_103 }}" data-speed="1000" data-fresh-interval="20"></div>
                            </div>
                        </div>
                    </div> 
                </div>
                <div class="row clearfix">
                <!-- Task Info -->
                    <div class="card">
                        <div class="header bg-red">
                            <h2>Task List</h2>
                        </div>
                        <div class="body">

                        
                            <div class="table-responsive">
                                <table class="table table-hover dashboard-task-infos" id="example4">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th width="20%">Name</th>
                                            <th>IC</th>
                                            <th>Phone</th>
                                            <th width="20%">Date</th>
                                            <th>Status</th>
                                            <th>Activity</th>
                                            <th>Detail</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        <?php $i = 1; ?>

                                        @foreach($tasklist_p2 as $data)
                                        <tr>
                                            <td>{{$i++}}</td>
                                            <td>{{$data->name}}</td>
                                            <td>{{$data->ic}}</td>
                                            <td>{{$data->notelp}}</td> 
                                            <td>{{$data->created_at->toDayDateTimeString()}}</td> 
                                            <td>
                                                @include('shared.stage_new')
                                            </td>
                                            <td>
                                                <a href="{{url('/history/show/'.$data->id_cus)}}" class="btn bg-orange btn-circle waves-effect waves-circle waves-float" onclick="window.open('{{url('/history/show/'.$data->id_cus)}}', 'newwindow', 'width=750,height=400'); return false;"> <i class="material-icons">history</i> </a>
                                            </td>
                                            <td>
                                               @if($data->stage == 'W4')
                                                <a href="{{ url('processor2/'.$data->id_cus.'/step1') }}" class="btn btn-sm bg-pink"><i class="material-icons">border_color</i> Loan Calculation </a>
                                                @endif
                                            </td> 
                                            
                                        </tr>
                                        @endforeach

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            @elseif($roless=='7')
            <div class="row clearfix">
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12" onclick="location.href='{{route('loan-eli.index')}}';" style="cursor:pointer; ">
                        <div class="info-box bg-pink hover-expand-effect">
                            <div class="icon">
                                <i class="material-icons">donut_small</i>
                            </div>
                            
                                <div class="content">
                                    <div class="text">MBSB Processing</div>
                                    <div class="number count-to" data-from="0" data-to="{{ $count_mbsb_processing_p3 }}" data-speed="1000" data-fresh-interval="20"></div>
                                </div>
                        </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12" onclick="location.href='{{route('loan-eli.index')}}';">
                    <div class="info-box bg-orange hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">rate_review</i>
                        </div>
                        <div class="content">
                            <div class="text">Additional Document</div>
                            <div class="number count-to" data-from="0" data-to={{$pen_doc_check}} data-speed="1000" data-fresh-interval="20"></div>
                        </div>
                    </div>
                </div> 
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12" onclick="location.href='{{route('loan-eli.index')}}';" style="cursor:pointer; ">
                    <div class="info-box bg-green hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">check</i>
                        </div>
                        <div class="content">
                            <div class="text">Disbursement</div>
                            <div class="number count-to" data-from="0" data-to="{{ $pen_103 }}" data-speed="1000" data-fresh-interval="20"></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12" onclick="location.href='{{route('loan-eli.index')}}';" style="cursor:pointer; ">
                    <div class="info-box bg-red hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">cancel</i>
                        </div>
                        <div class="content">
                            <div class="text">Rejected</div>
                            <div class="number count-to" data-from="0" data-to="{{ $pen_103 }}" data-speed="1000" data-fresh-interval="20"></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row clearfix">
                <!-- Task Info -->
                    <div class="card">
                        <div class="header bg-red">
                            <h2>Task List</h2>
                        </div>
                        <div class="body">

                        
                            <div class="table-responsive">
                                <table class="table table-hover dashboard-task-infos" id="example4">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th width="20%">Name</th>
                                            <th>IC</th>
                                            <th>Phone</th>
                                            <th width="20%">Date</th>
                                            <th>Status</th>
                                            <th>Activity</th>
                                            <th>Detail</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        <?php $i = 1; ?>

                                        @foreach($tasklist_p3 as $data)
                                        <tr>
                                            <td>{{$i++}}</td>
                                            <td>{{$data->name}}</td>
                                            <td>{{$data->ic}}</td>
                                            <td>{{$data->notelp}}</td> 
                                            <td>{{$data->created_at->toDayDateTimeString()}}</td> 
                                            <td>
                                                @include('shared.stage_new')
                                            </td>
                                            <td>
                                                <a href="{{url('/history/show/'.$data->id_cus)}}" class="btn bg-orange btn-circle waves-effect waves-circle waves-float" onclick="window.open('{{url('/history/show/'.$data->id_cus)}}', 'newwindow', 'width=750,height=400'); return false;"> <i class="material-icons">history</i> </a>
                                            </td>
                                            <td>
                                               @if($data->stage == 'W4')
                                                <a href="{{ url('processor2/'.$data->id_cus.'/step1') }}" class="btn btn-sm bg-pink"><i class="material-icons">border_color</i> Loan Calculation </a>
                                                @endif
                                            </td> 
                                            
                                        </tr>
                                        @endforeach

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            @endif


        </div>

        <!-- Gak jadi dulu -->
        <!-- modal detail -->
        @include('shared._modaldetail')
        <!-- end of modal detail -->


    </section>

    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>

    <script>
        $(document).ready(function() {
        $('#example').DataTable();
        });
    </script>

    <script type="text/javascript">
        
        $(document).ready(function() {
        $('#example2').DataTable();
        });
    </script>

    <script>
        
        $(document).ready(function() {
        $('#example4').DataTable();
        });
    </script>

@endsection