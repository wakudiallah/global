@extends('vadmin.tampilan')


@section('content')
    <section class="content">
        <div class="container-fluid">
            
            <div class="row clearfix"> <!-- Breadcrumber -->
                <div class="col-md-6">
                    <ol class="breadcrumb breadcrumb-col-pink">
                        <li><a href="javascript:void(0);"><i class="material-icons">person_pin</i>Add New Customer</a></li>
                        <li class="active"><i class="material-icons">create</i> Add</li>
                    </ol>
                </div>
            </div> <!-- End of breadcrumber -->
            
            <!-- Advanced Form Example With Validation -->

            @if(Session::has('message'))
                <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
            @endif

            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header bg-red">
                            <h2>Add New Customer</h2>
                            
                        </div>
                        <div class="body">

                            <h1 class="card-inside-title">Customer Information</h1>
                            <div class="row clearfix">
                                <div class="col-sm-12">


                                    <fieldset>

                                    {!! Form::open(['route' => ['cus_mo.store'], 'class' => "probootstrap-form border border-danger", 'id' => 'form-validate-pra']) !!}

                                    {{ csrf_field() }}

                                    <span id="latitude"></span>
                                    <span id="longitude"></span>
                                    <span id="location"></span>

                                    <div class="form-group form-float">
                                        <b>Full Name :</b>
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="name" id="name" required @if (Session::has('name'))  value="{{ Session::get('name') }}" @endif>
                                        </div>   
                                    </div>
                                    <div class="form-group form-float">
                                        <b>IC Number :</b>
                                        <div class="form-line">
                                            <input type="text"  class="form-control" name="ic" id="ic"  min="1" oninput="this.value=this.value.replace(/[^0-9]/g,'');" maxlength="12"  required @if (Session::has('ic'))  value="{{ Session::get('ic') }}" @endif>
                                        </div>   
                                    </div>
                                    <!-- <div class="form-group form-float">
                                        <div class="form-line">
                                            <label class="form-label">Old IC (If Any) :</label>
                                            <input type="text"  class="form-control" name="old_ic" id="ic" value=""  min="1" oninput="this.value=this.value.replace(/[^0-9]/g,'');"  value=${var} ">
                                        </div>   
                                    </div> -->
                                    
                                    <div class="form-group form-float">
                                        <b>Phone Number :</b>
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="notelp" id="notelp"  min="1" oninput="this.value=this.value.replace(/[^0-9]/g,'');" maxlength="15" minlength="9" required @if (Session::has('notelp'))  value="{{ Session::get('notelp') }}" @endif>
                                        </div>    
                                    </div>
                                    <!-- <div class="form-group form-float">
                                        <div class="form-line">
                                            <label class="form-label">Job Type :</label>
                                            
                                            <select class="form-control" id="employment_code" name="employment_code">
                                              @foreach ($employment as $data)
                                              <option value="{{$data->code_employment}}">{{$data->name}}</option>
                                              @endforeach                          
                                            </select>
                                        </div>    
                                    </div> 
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <label class="form-label">Employer :</label>
                                            <select class="js-example-basic-single js-states form-control" id="emp_code" name="emp_code">
                                              @foreach ($emp as $data)
                                              <option value="{{$data->id}}">{{$data->Emp_Desc}}</option>
                                              @endforeach                          
                                            </select>
                                        </div>    
                                    </div>
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <label class="form-label">Basic Salary (RM):</label>
                                            <input type="text" id="gaji_asas" name="gaji_asas" class="form-control" value="" min="1" oninput="this.value=this.value.replace(/[^0-9]/g,'');"  value=${var} ">
                                        </div>    
                                    </div>  
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <label class="form-label">Allowance (RM):</label>
                                            <input type="text" class="form-control" value="" id="elaun" name="elaun" min="1" oninput="this.value=this.value.replace(/[^0-9]/g,'');"  value=${var} >
                                        </div>    
                                    </div>
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <label class="form-label">Total Deduction (RM):</label>
                                            <input type="text" class="form-control" value="" id="pot_bul" name="pot_bul" min="1" oninput="this.value=this.value.replace(/[^0-9]/g,'');"  value=${var} >
                                        </div>    
                                    </div> 
                                <div class="form-group form-float">
                                        <div class="form-line">
                                            <label class="form-label">Job Type :</label>
                                            
                                            <select class="form-control" id="employment_code" name="employment_code">
                                              @foreach ($employment as $data)
                                              <option value="{{$data->code_employment}}">{{$data->name}}</option>
                                              @endforeach                          
                                            </select>
                                        </div>    
                                    </div>-->

                                    <div class="form-group form-float">
                                        <b>Email :</b>
                                        <div class="form-line">
                                            <input type="email" class="form-control" name="email" id="email" required @if (Session::has('email'))  value="{{ Session::get('email') }}" @endif>
                                        </div>   
                                    </div>

                                    <div class="form-group form-float">
                                        <label class="form-label">Employer :</label>
                                        
                                            <select class="form-control show-tick js-example-basic-single js-states" data-live-search="true" name="emp_code" id="one" required>
                                                <option value="" selected disabled hidden>Choose Employer</option>
                                                @foreach ($emp as $data)
                                                  <option value="{{$data->id}}">{{$data->Emp_Desc}}</option>
                                                @endforeach  
                                            </select>
                                    </div>
                                    <!-- <div class="form-group form-float">
                                        <label class="form-label">Employer :</label>
                                        <div class="form-line">
                                            
                                            <select class="js-example-basic-single js-states form-control show-tick" id="one" name="emp_code" required>
                                              <option value="" selected disabled hidden>Choose Employer</option>
                                              @foreach ($emp as $data)
                                              <option value="{{$data->id}}">{{$data->Emp_Desc}}</option>
                                              @endforeach                          
                                            </select>
                                        </div>    
                                    </div> -->

                                    <div class="form-group form-float resources" style="display: none" id="two">
                                        <input name="clerical" type="radio" id="radio_1" value="1" checked />
                                        <label for="radio_1">Clerical</label>
                                        <input name="clerical" type="radio" id="radio_2" value="0" />
                                        <label for="radio_2">Non Clerical</label>
                                    </div>


                                    
                                    <!-- 
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <label class="form-label">Loan Amount (RM):</label>
                                            <input type="text" class="form-control" id="jml_pem" name="jml_pem" value="" min="1" oninput="this.value=this.value.replace(/[^0-9]/g,'');"  value=${var} ">
                                        </div>    
                                    </div> -->

                                </div>
                            </div>

                            <div class="row clearfix">
                                <div class="col-md-10"></div>
                                <div class="col-md-2">
                                    <input type="submit" value="Submit" class="btn btn-lg btn-success btn-block" style="cursor:pointer;">
                                        {{ csrf_field() }}
                                </div>
                            </div>
                            
                            
                                    
                            {!! Form::close() !!}

                            </fieldset>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Advanced Form Example With Validation -->

        </div>
    </section>


    @push('js')
    <script type="text/javascript">
        var Privileges = jQuery('#one');
        var select = this.value;
        Privileges.change(function () {
            if ($(this).val() == '1') {
                $('.resources').show();
            }
            else $('.resources').hide();
        });
    </script>
    @endpush


@endsection