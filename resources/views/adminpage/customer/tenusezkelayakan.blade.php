@extends('vadmin.tampilan')

@section('content')
	<section class="content">
        <div class="container-fluid">
            
            <div class="row clearfix"> <!-- Breadcrumber -->
                <div class="col-md-6">
                    <ol class="breadcrumb breadcrumb-col-pink">
                        <li><a href="javascript:void(0);"><i class="material-icons">person_pin</i> Customer</a></li>
                        <li class="active"><i class="material-icons">create</i> Loan Eligibility</li>
                    </ol>
                </div>
            </div> <!-- End of breadcrumber -->
            
            

            <!-- Advanced Form Example With Validation -->
            <div class="row clearfix">
            	<div class="col-md-2"></div>
            	<div class="col-md-8">
	                <div class="card">
	                    <div class="header bg-red">
	                        <h2>Loan Eligibility</h2>
	                    </div>
	                    <div class="body">

	                		<div class="row clearfix">
				                <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
				                    <button class="btn btn-success btn-lg btn-block waves-effect" type="button" style="line-height: 40px !important"><b>Name</b>: {{$pra->name}} </button>
				                </div>
				                <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
				                    <button class="btn btn-primary btn-lg btn-block waves-effect" type="button" style="line-height: 40px !important"><b>IC Number</b>: {{$pra->ic}} </button>
				                </div>
				                <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
				                    <button class="btn btn-danger btn-lg btn-block waves-effect" type="button" style="line-height: 40px !important"><b>Phone Number</b>: {{$pra->notelp}} </button>
				                </div>
				                <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
				                    <button class="btn btn-warning btn-lg btn-block waves-effect" type="button" style="line-height: 40px !important"><b>Email</b>: {{$pra->email}} </button>
				                </div>
				            </div>

	                    	{!! Form::open(['url' => ['/tenos/custtenos/store'], 'class' => "probootstrap-form border border-danger", 'method' => 'post', 'id' => 'form-validate-pra']) !!}

	                        {{ csrf_field() }}

	                        <input type="hidden" name="id_cus" value="{{$pra->id_cus}}">

	                    	<label for="">Job Type</label>
	                        <div class="form-group">
	                            <div class="form-line">
	                                <select class="form-control" id="employment_code" name="employment_code">
	                                  @foreach ($employment as $data)
	                                  <option value="{{$data->code_employment}}">{{$data->name}}</option>
	                                  @endforeach                          
	                                </select>
	                            </div>
	                        </div>

	                        <div class="form-group form-float" style="margin-top: 60px !important">
                                <label class="form-label">Package:</label>
                                <div class="form-line">
                                    
                                    <select class="js-example-basic-single js-states form-control" id="loanpkg_code" name="loanpkg_code">
                                      @foreach ($loanpkg as $data)
                                      <option value="{{$data->LnPkg_Code}}">{{$data->Ln_Desc}}</option>
                                      @endforeach
                                    </select>
                                </div>    
                            </div>

	                        <label for="">Job Status</label>
	                        <div class="form-group">
	                            <div class="form-line">
	                                <select class="form-control" id="job_status" name="job_status">
	                                  
	                                  <option value="1">Permanent</option>
	                                  <option value="2">Contract</option>                        
	                                </select>
	                            </div>
	                        </div>
	                    	<label for="">Basic Salary (RM):</label>
	                        <div class="form-group">
	                            <div class="form-line">
	                                <input type="text" id="gaji_asas" name="gaji_asas" class="form-control" value="" min="1" oninput="this.value=this.value.replace(/[^0-9]/g,'');"  value=${var} " required>
	                            </div>
	                        </div>
	                        <label for="">Allowance (RM):</label>
	                        <div class="form-group">
	                            <div class="form-line">
	                                <input type="text" class="form-control" value="" id="elaun" name="elaun" min="1" oninput="this.value=this.value.replace(/[^0-9]/g,'');"  value=${var} required>
	                            </div>
	                        </div>
	                        <label for="">Total Deduction (RM):</label>
	                        <div class="form-group">
	                            <div class="form-line">
	                                <input type="text" class="form-control" value="" id="pot_bul" name="pot_bul" min="1" oninput="this.value=this.value.replace(/[^0-9]/g,'');"  value=${var} required>
	                            </div>
	                        </div>
	                        <label for="">Loan Amount (RM):</label>
	                        <div class="form-group">
	                            <div class="form-line">
	                                <input type="text" class="form-control" id="jml_pem" name="jml_pem" value="" min="1" oninput="this.value=this.value.replace(/[^0-9]/g,'');"  value=${var} " required>
	                            </div>
	                        </div>

	                         <div class="row">
	                            <div class="col-md-10"></div>
	                            <div class="col-md-2">
	                                <input type="submit" value="Submit" class="btn btn-lg btn-success btn-block" style="cursor:pointer;">
	                                    {{ csrf_field() }}
	                            </div>
	                        </div>
	                    
	                     {!! Form::close() !!}
	                    </div>
	                </div>
	            </div>
	            <div class="col-md-2"></div>
            </div>

        </div>
    </section>
@endsection