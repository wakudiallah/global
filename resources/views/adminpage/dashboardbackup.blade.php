<div class="row clearfix">

    <!-- Visitors -->
    <!-- <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
        <div class="card">
            <div class="body bg-pink">
                <div class="sparkline" data-type="line" data-spot-Radius="4" data-highlight-Spot-Color="rgb(233, 30, 99)" data-highlight-Line-Color="#fff"
                     data-min-Spot-Color="rgb(255,255,255)" data-max-Spot-Color="rgb(255,255,255)" data-spot-Color="rgb(255,255,255)"
                     data-offset="90" data-width="100%" data-height="92px" data-line-Width="2" data-line-Color="rgba(255,255,255,0.7)"
                     data-fill-Color="rgba(0, 188, 212, 0)">
                    12,10,9,6,5,6,10,5,7,5,12,13,7,12,11
                </div>
                <ul class="dashboard-stat-list">
                    <li>
                        TODAY
                        <span class="pull-right"><b>1 200</b> <small>USERS</small></span>
                    </li>
                    <li>
                        YESTERDAY
                        <span class="pull-right"><b>3 872</b> <small>USERS</small></span>
                    </li>
                    <li>
                        LAST WEEK
                        <span class="pull-right"><b>26 582</b> <small>USERS</small></span>
                    </li>
                </ul>
            </div>
        </div>
    </div> -->
    <!-- #END# Visitors -->
    <!-- Latest Social Trends -->
    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
        <div class="card">
            <div class="body bg-cyan">
                <div class="m-b--35 font-bold">Top 5 Majikan</div>
                <ul class="dashboard-stat-list">  
                    @foreach($bestmajikan as $data)
                        <li>
                            @if(empty($data->emp_code))
                                <span class="pull-right"><b>0</b> <small>employer</small></span>
                            @else
                            {{$data->majikan->Emp_Desc}}
                                <span class="pull-right"><b>{{$data->total}}</b> <small>Submittion</small></span>
                            @endif
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
    <!-- #END# Latest Social Trends -->
    <!-- Latest Social Kuning Task -->
    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
        <div class="card">
            <div class="body bg-orange">
                <div class="m-b--35 font-bold">Top 5 Agent By Month</div>
                <ul class="dashboard-stat-list">
                    @foreach($bestagentmonth as $data)
                    <li>
                        {{$data->user->name}}
                        <span class="pull-right"><b>{{$data->total}}</b> <small>Cus</small></span>
                    </li>
                    @endforeach
                        <!-- <span class="pull-right">
                            <span class="pull-right"><b>12</b> <i class="material-icons">trending_up</i></span>
                        </span> -->
                </ul>
            </div>
        </div>
    </div>
    <!-- #END# Latest Social Trends -->
    
</div>

<!-- Top MO -->

<div class="row clearfix">
    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
        <div class="card">
            <div class="body bg-purple">
                <div class="m-b--35 font-bold">Top 5 Agent (Assessment) By Years</div>
                <ul class="dashboard-stat-list">
                    <li>
                        Budi
                        <span class="pull-right"><b>100</b> <small>Asses</small></span>
                    </li>
                    <li>
                        Andi
                        <span class="pull-right"><b>89</b> <small>Asses</small></span>
                    </li>
                    <li>
                        Ilham
                        <span class="pull-right"><b>70</b> <small>Asses</small></span>
                    </li>
                    <li>
                        Fery
                        <span class="pull-right"><b>30</b> <small>Asses</small></span>
                    </li>
                    <li>
                        Rahman Ar
                        <span class="pull-right"><b>25</b> <small>Asses</small></span>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
        <div class="card">
            <div class="body bg-pink">
                <div class="m-b--35 font-bold">Top 5 Agent (Submission) By Years</div>
                <ul class="dashboard-stat-list">
                    <li>
                        Budi
                        <span class="pull-right"><b>100</b> <small>Subm</small></span>
                    </li>
                    <li>
                        Andi
                        <span class="pull-right"><b>89</b> <small>Subm</small></span>
                    </li>
                    <li>
                        Ilham
                        <span class="pull-right"><b>70</b> <small>Subm</small></span>
                    </li>
                    <li>
                        Fery
                        <span class="pull-right"><b>30</b> <small>Subm</small></span>
                    </li>
                    <li>
                        Rahman Ar
                        <span class="pull-right"><b>25</b> <small>Subm</small></span>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
        <div class="card">
            <div class="body bg-green">
                <div class="m-b--35 font-bold">Top 5 Agent (Disbursement) By Years</div>
                <ul class="dashboard-stat-list">
                    <li>
                        Tono
                        <span class="pull-right"><b>80</b> <small>Disb</small></span>
                    </li>
                    <li>
                        Susi
                        <span class="pull-right"><b>77</b> <small>Disb</small></span>
                    </li>
                    <li>
                        Ilham
                        <span class="pull-right"><b>70</b> <small>Disb</small></span>
                    </li>
                    <li>
                        Budi
                        <span class="pull-right"><b>30</b> <small>Disb</small></span>
                    </li>
                    <li>
                        Rahman Ar
                        <span class="pull-right"><b>25</b> <small>Disb</small></span>
                    </li>
                </ul>
            </div>
        </div>
    </div>

</div>


<div class="row clearfix">
    <!-- Task Info -->
    <!-- <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="card">
            <div class="header bg-red">
                <h2>Loans</h2>
            </div>
            <div class="body">
                <div class="table-responsive">
                    <table class="table table-hover dashboard-task-infos" id="example">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>IC</th>
                                <th>Date Received</th>
                                <th>Date Submitted</th>
                                <th>Lokasi</th>
                                <th>Remark</th>
                                <th>History</th>
                                <th>Action</th>
                                <th>Doc</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i = 1; ?>

                            @foreach($praaplication as $data)
                            <tr>
                                <td>{{$i++}}</td>
                                <td>{{$data->name}}</td>
                                <td>{{$data->ic}}</td>
                                <td>{{$data->created_at}}</td>
                                <td></td>
                                <td><a href="{{url('/location/show/'.$data->id)}}" class="btn bg-light-blue btn-circle waves-effect waves-circle waves-float" onclick="window.open('{{url('/location/show/'.$data->id)}}', 'newwindow', 'width=600,height=400'); return false;"> <i class="material-icons">place</i> </a></td>
                                
                                <td></td>
                                
                                <td class="text-center">
                                    <a href="{{url('/history/show/'.$data->id_cus)}}" class="btn bg-orange btn-circle waves-effect waves-circle waves-float" onclick="window.open('{{url('/history/show/'.$data->id_cus)}}', 'newwindow', 'width=750,height=400'); return false;"> <i class="material-icons">history</i> </a>
                                </td>
                                <td>
                                  
                                    <a href="{{url('/doc/verify/'.$data->id_cus)}}" class="btn btn-primary"><i class="material-icons disabled">fingerprint</i> Verify App </a>
                                </td>

                                <td>
                                    @if((empty($data->doc->cus_id))) 
                                    <a href="{{url('/doc/submit/'.$data->id_cus)}}" class="btn bg-red"><i class="material-icons disabled">do_not_disturb_alt</i> </a>
                                    @else
                                    <a href="{{url('/doc/show/'.$data->id_cus)}}" class="btn bg-light-blue" target='_blank'> <i class="material-icons">library_books</i> </a></td>
                                    @endif
                                   
                            </tr>
                            @endforeach
                            
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div> -->
    <!-- #END# Task Info -->
    
</div>