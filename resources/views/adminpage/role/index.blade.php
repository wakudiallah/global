@extends('vadmin.tampilan')

@section('content')

	<section class="content">
        <div class="container-fluid">

            <div class="row clearfix"> <!-- Breadcrumber -->
                <div class="col-md-6">
                    <ol class="breadcrumb breadcrumb-col-pink">
                        <li><a href="javascript:void(0);"><i class="material-icons">person</i> User</a></li>
                        <li class="active"><i class="material-icons">pan_tool</i> Work Group</li>
                    </ol>
                </div>
            </div> <!-- End of breadcrumber -->
        
            <!-- Modal -->
		    <div class="modal fade" id="roleModal" tabindex="-1" role="dialog" aria-labelledby="roleModalLabel">
		        <div class="modal-dialog" role="document">
		            {!! Form::open(['method' => 'post']) !!}

		            <div class="modal-content card">
		                <div class="modal-header bg-red">
		                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		                    <h4 class="modal-title" id="roleModalLabel">Work Group</h4>
		                </div>
		                <div class="modal-body body">
		                    <!-- name Form Input -->
		                    <div class="form-group @if ($errors->has('name')) has-error @endif">
		                        <div class="form-line">
		                        {!! Form::label('name', 'Name') !!}
		                        {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Work Group']) !!}
		                        </div>
		                        @if ($errors->has('name')) <p class="help-block">{{ $errors->first('name') }}</p> @endif
		                    </div>
		                </div>
		                <div class="modal-footer">
		                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

		                    <!-- Submit Form Button -->
		                    {!! Form::submit('Submit', ['class' => 'btn btn-primary']) !!}
		                </div>
		                {!! Form::close() !!}
		            </div>
		        </div>
		    </div> <!-- End of modal -->

		    <div class="row clearfix demo-button-sizes">
		        <div class="col-md-10">
		            
		        </div>
		        <div class="col-md-2 page-action text-right">
		            @can('add_roles')
		                <a href="#" class="btn btn-success btn-block btn-sm waves-effect" data-toggle="modal" data-target="#roleModal"> Add</a>
		                <!-- <div class="row  ">
					        <div class="col-md-10">
					            <h3></h3>
					        </div>
					        <div class="col-md-2 col-xs-6 col-sm-3 col-lg-2 page-action text-right">
					            <a href="" class="btn bg-red btn-block btn-sm waves-effect"> <i class="fa fa-arrow-left"></i> Back</a>
					        </div>
					    </div> -->
		            @endcan
		        </div>
		    </div>


		    @forelse ($roles as $role)
		        {!! Form::model($role, ['method' => 'PUT', 'route' => ['roles.update',  $role->id ], 'class' => 'm-b']) !!}

		        
		            @include('shared._permissions', [
		                          'title' => $role->name .' Permissions',
		                          'model' => $role ])

		            @can('edit_roles')
		                {!! Form::submit('Save', ['class' => 'btn btn-primary btn-lg', 'style' => 'margin-bottom : 20px !important' ]) !!}

		            @endcan
		       

		        {!! Form::close() !!}

		    @empty
		        <p>No Roles defined, please run <code>php artisan db:seed</code> to seed some dummy data.</p>
		    @endforelse

        </div>
    </section>

@endsection