@extends('vadmin.tampilan')

@section('content')

	<section class="content">
        <div class="container-fluid">

            <div class="row clearfix"> <!-- Breadcrumber -->
                <div class="col-md-6">
                    <ol class="breadcrumb breadcrumb-col-pink">
                        <li><a href="javascript:void(0);"><i class="material-icons">assignment</i> Reporting</a></li>
                        <li class="active"><i class="material-icons">playlist_add_check</i> Report</li>
                    </ol>
                </div>
            </div> <!-- End of breadcrumber -->

            <div class="row clearfix">
            	<div class="col-md-2"></div>
            	<div class="col-md-8">
            		<div class="card">
			            <div class="header bg-red">
			                <h2>Report</h2>
			            </div>
			            <div class="body">

		            			<h2 class="card-inside-title">Status</h2>
		            			<select name="remarkp6" class="form-control" id="one">
                                    <option value="" selected disabled hidden>Choose Status</option>
                                    <option value="W9">All </option>
                                    <option value="W9">Approved </option>
                                    <option value="W6">Rejected <!-- Calculation & Changes --></option>
                                </select>

                                <h2 class="card-inside-title">From Date</h2>

                                	<input type="text" class="form-control" placeholder="From Date" />
                                	<input type="text" class="form-control"  id="datepicker" />

                                
                                <h2 class="card-inside-title">Date To</h2>
                                <input type="text" class="form-control" placeholder="Date To" />


								<button class="btn btn-success" aria-controls="collapse" data-target="#collapseDetailOne1" data-toggle="collapse" style="cursor:pointer; margin-top: 20px !important"><i class="material-icons">visibility</i> Generate</button>
                                

			            </div>
			        </div>
            	</div>
		        <div class="col-md-2"></div>
			</div>


			<div id="collapseDetailOne1" class="collapse" aria-expanded="false" data-collapse-group="collapse-group" aria-labelledby="headingFive" data-parent="#accordionExample">
                <div class="row clearfix">
                    <div class="card">
                        <div class="header bg-red">
                            <h2>Report</h2>
                        </div>
                        <div class="body">
                        	<button class="btn btn-success"  style="cursor:pointer; margin-top: 20px !important"><i class="material-icons">print</i> Export</button>

                            <div class="table-responsive">
                                <table class="table table-hover dashboard-task-infos" id="example">
                                    <thead>
                                        <tr>
                                            <th width="10%">#</th>
                                            <th width="30%">Name</th>
                                            <th width="20%">IC</th>
                                            <th>Phone</th>
                                            <th>Email</th>
                                            <th>Employer</th>
                                            <th>Basic Salary</th>
                                            <th>Loan Ammount</th>
                                            <th>Loan Approve</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    	<tr>
                                    		<td></td>
                                    		<td></td>
                                    		<td></td>
                                    		<td></td>
                                    		<td></td>
                                    		<td></td>
                                    		<td></td>
                                    		<td></td>
                                    		<td></td>
                                    	</tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                            
                    </div>
                </div>
            </div>

		    
        </div>
    </section>

    

    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>



    <script src="https://cdn.jsdelivr.net/npm/gijgo@1.9.10/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://cdn.jsdelivr.net/npm/gijgo@1.9.10/css/gijgo.min.css" rel="stylesheet" type="text/css" />

	
	<script type="text/javascript">
	var table = $('#example').DataTable({
	                lengthChange: false,
	                buttons: [ 'copy', 'excel', 'pdf', 'colvis' ]
	            });
	table.buttons().container().appendTo( '#example_wrapper .col-sm-6:eq(0)' );
	</script>

	
    <script>
        $('#datepicker').datepicker({
            uiLibrary: 'bootstrap4'
        });
    </script>

    <script>
        $('#datepicker2').datepicker({
            uiLibrary: 'bootstrap4'
        });
    </script>


@endsection