@extends('vadmin.tampilan')

@section('content')
	<section class="content">
        <div class="container-fluid">

            <div class="row clearfix"> <!-- Breadcrumber -->
                <div class="col-md-6">
                    <ol class="breadcrumb breadcrumb-col-pink">
                        <li><a href="javascript:void(0);"><i class="material-icons">assignment</i> Reporting</a></li>
                        <li class="active"><i class="material-icons">event</i> Pengiraan / Calculation</li>
                    </ol>
                </div>
            </div> <!-- End of breadcrumber -->

            <div class="row clearfix demo-button-sizes">
                <div class="col-md-10"></div>
                
                <div class="col-xs-6 col-sm-3 col-md-2 col-lg-2">
                    <a href="{{ route('cal.create') }}" type="submit" class="btn btn-success btn-block btn-lg waves-effect" style="cursor:pointer;">
                      Tambah
                    </a>
                </div>
                
            </div>

		    <div class="row clearfix">
                <div class="col-md-12">
                    <div class="card">
                        <div class="header bg-red">
                            <h2>Pengiraan / Calculation </h2>
                        </div>
                        <div class="body">
                            <table class="table" id="example">
                                <thead>
                                    <tr>
                                    	
                                        <th>#</th>
                                        <th>Date Start</th>
						                <th>Date Stop</th>
						                <th>Deskripsi</th>
						                <th>Status</th>
						                
						                <th class="text-center">Aksi</th>
						                
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i = 1; ?>

                                	@foreach($cal as $data)
					                <tr>
					                    <td>{{$i++}}</td>
                                        <td>{{ $data->dt_start }}</td>
					                    <td>{{ $data->dt_stop }}</td>
					                    <td>{{ $data->desc }}</td>
					                    
					                    <td><div class="switch">
		                                	    <label>Non Act<input type="checkbox" id="aksi" name="status" checked><span class="lever switch-col-red"></span>Active</label>
                                            </div>
		                            	</td>

					                    
					                    <td class="text-center">
					                        @include('shared._actions', [
					                            'entity' => 'users',
					                            'id' => $data->id
					                        ])
					                    </td>
					                    
					                </tr>
					            @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

		    
        </div>
    </section>

    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>

    <!-- jquery for status -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function(){
            $('#aksi').change(function() {
            var returnVal = confirm("Are you sure?"); 
                if(returnVal){
                  postToServer($(this).prop("checked"))
                }else{
                  $(this).prop("checked", !$(this).is(":checked"));
                }       
            });
        });

        function postToServer(state){
            let value = (state) ? 1 : 0;
            alert('Posted Value: ' + value);
            $.ajax({
              type: "PUT",
              url: "users/status",
              data: {"aski":value},
              success: function(response){
                //handle response
              }
            });

        }
    </script>
    <!-- End status -->

    <!-- datatable -->
    <script>
	    $(document).ready(function() {
	    $('#example').DataTable();
	    });
	</script>

@endsection