@extends('vadmin.tampilanmin')

@section('content')

	<section class="content">
        <div class="container-fluid">        

	    	<!-- Tabs With Only Icon Title -->
	        <div class="row clearfix">
	            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	                <div class="card">
	                    <div class="header bg-red">
	                        <h2>
	                            Activity {{$praapplication->name}} ({{$praapplication->ic}})
	                        </h2>
	                    </div>
	                    <div class="body">
	                    	<div class="table-responsive">
	                            <table class="table table-hover dashboard-task-infos" id="example">
	                                <thead>
	                                    <tr>
	                                        <th>Date</th>
	                                        <th>Status</th>
	                                        <th>By</th>
	                                        <th>Note</th>
	                                    </tr>
	                                </thead>
	                                <tbody>

	                                	@foreach($history as $data)
	                                    <tr>
	                                        <!-- {{Carbon\Carbon::parse($data->created_at)	->format('d-m-Y i')}} -->
	                                        <td>{{$data->created_at->toDayDateTimeString()}}</td>
	                                        <td>
	                                        	<span class="label bg-{{$data->stage->color}}">{{$data->stage->desc}}</span>
	                                        </td>
	                                        <td>{{$data->user->name}}</td>
	                                        <td>{{$data->note}}</td>
	                                    </tr>
	                                    @endforeach
	                                </tbody>
	                            </table>
	                        </div>
	                    </div>
	                </div>
	            </div>
	        </div>
	    </div>
	</section>

	<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>

    <script>
        $(document).ready(function() {
        $('#example').DataTable();
        });
    </script>	

@endsection