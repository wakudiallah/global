@extends('vadmin.tampilan')

@section('content')

    <section class="content">
        <div class="container-fluid">

            <div class="row clearfix"> <!-- Breadcrumber -->
                <div class="col-md-6">
                    <ol class="breadcrumb breadcrumb-col-pink">
                        <li><a href="javascript:void(0);"><i class="material-icons">home</i> Dashboard</a></li>
                    </ol>
                </div>
            </div> <!-- End of breadcrumber -->

            <!-- Running Text -->
                <div class="info-box-3 bg-red hover-zoom-effect">
                    <div class="icon">
                        <i class="material-icons">email</i>
                    </div>
                    <div class="content">
                        <div class="headline-text">
                            @if(empty($announc->id))
                                <marquee scrollamount="5" width="1000px" onmouseover="stop();" onmouseout="start();" ></marquee>
                            @else
                                <marquee scrollamount="5" width="1000px" onmouseover="stop();" onmouseout="start();" ><a href="{{url('/announc/show/'.$announc->id)}}" style="color:white; font-size: 20px; line-height: 60px !important; text-decoration: none;" onMouseOver="this.style.color='#a39999'" onMouseOut="this.style.color='#FFF'" > {{$announc->title}} </a></marquee>
                            @endif
                        </div>
                    </div>
                    
                </div> <!-- End of running text -->



           


       
                
            <!-- Widgets -->  <!-- Role = User -->
            
            @if( $roless=='1') <!-- =====================  Task Admin  ================== -->
            <div class="row clearfix">
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box bg-pink hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">forum</i>
                        </div>

                        <div class="content">
                            <div class="text">Assessment</div>
                            <div class="number count-to" data-from="0" data-to="{{ $countuser }}" data-speed="15" data-fresh-interval="20"></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box bg-cyan hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">person_add</i>
                        </div>
                        <div class="content">
                            <div class="text">Submission</div>
                            <div class="number count-to" data-from="0" data-to="{{ $countsub }}" data-speed="1000" data-fresh-interval="20"></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box bg-light-green hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">playlist_add_check</i>
                        </div>
                        <div class="content">
                            <div class="text">Disbursement</div>
                            <div class="number count-to" data-from="0" data-to="0" data-speed="1000" data-fresh-interval="20"></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box bg-orange hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">help</i>
                        </div>
                        <div class="content">
                            <div class="text">Pending</div>
                            <div class="number count-to" data-from="0" data-to="0" data-speed="1000" data-fresh-interval="20"></div>
                        </div>
                    </div>
                </div>
            </div>  <!-- ===================== End Task Admin ==================== -->
            <!-- #END# Widgets --> 
            @elseif($roless=='3')      <!-- =====================  Task MO  ================== -->
            <div class="row clearfix">
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box bg-pink hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">playlist_add_check</i>
                        </div>

                        <div class="content">
                            <div class="text">Submission</div>
                            <div class="number count-to" data-from="0" data-to="{{ $countuser }}" data-speed="15" data-fresh-interval="20"></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" onclick="location.href='{{route('loan-eli.index')}}';">
                    <div class="info-box bg-cyan hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">work</i>
                        </div>
                        <div class="content">
                            <div class="text">Calculation & Checking</div>
                            <div class="number count-to" data-from="0" data-to="{{ $countmo_calculation }}" data-speed="1000" data-fresh-interval="20"></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box bg-light-green hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">local_offer</i>
                        </div>
                        <div class="content">
                            <div class="text">Disbursement</div>
                            <div class="number count-to" data-from="0" data-to="0" data-speed="1000" data-fresh-interval="20"></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" onclick="location.href='{{route('docincomplete.index')}}';" >
                    <div class="info-box bg-orange hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">help</i>
                        </div>
                        <div class="content">
                            <div class="text">Rejected</div>
                            <div class="number count-to" data-from="0" data-to={{$countreject}} data-speed="1000" data-fresh-interval="20"></div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="row clearfix">
                <!-- Task Info -->
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="card">
                        <div class="header bg-red">
                            <h2>Task list</h2>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-hover dashboard-task-infos" id="example">
                                    <thead>
                                        <tr>
                                            <th class="hidden"></th>
                                            <th>#</th>
                                            <th>Name</th>
                                            <th>IC</th>
                                            <th>Phone</th>
                                            <th>Date Received</th>
                                            <th>Status</th>
                                            <th>Activity</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        <?php $i = 1; ?>

                                        @foreach($meetcus as $data)

                                            <?php 
                                                $date_rev =  date('Y-m-d H:i:s ', strtotime($data->created_at));
                                                $today    =  date('Y-m-d H:i:s');
                                                $to_time = strtotime($today);
                                                $from_time = strtotime($date_rev);
                                                $di= ($to_time - $from_time) / (60*60);
                                            ?> 

                                        <tr>
                                            <td class="hidden">{{number_format($di,0,'.','.')}}</td>
                                            <td>{{$i++}}</td>
                                            <td>{{$data->name}}</td>
                                            <td>{{$data->ic}}</td>
                                            <td>{{$data->notelp}}</td>

                                            <td>
                                                {{$data->created_at->toDayDateTimeString()}}
                                            </td>
                                            <td>
                                                <span class="label bg-{{$data->stages->color}}">{{$data->stages->desc}}</span>
                                            </td>
                                            
                                            
                                            <td class="text-center">
                                                <a href="{{url('/history/show/'.$data->id_cus)}}" class="btn bg-orange btn-circle waves-effect waves-circle waves-float" onclick="window.open('{{url('/history/show/'.$data->id_cus)}}', 'newwindow', 'width=750,height=400'); return false;"> <i class="material-icons">history</i> </a>
                                            </td>
                                            
                                               
                                        </tr>
                                        @endforeach
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- #END# Task Info -->
            </div>  <!-- ===================== End Task MO ==================== -->

            @elseif($roless=='5') <!-- Manager -->

            <div class="row clearfix">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="info-box bg-pink hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">close</i>
                        </div>

                        <div class="content">
                            <div class="text">Rejected</div>
                            <div class="number count-to" data-from="0" data-to="{{ $countmanager }}" data-speed="15" data-fresh-interval="20"></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="info-box bg-cyan hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">person_add</i>
                        </div>
                        <div class="content">
                            <div class="text">Submission</div>
                            <div class="number count-to" data-from="0" data-to="{{ $countsub }}" data-speed="1000" data-fresh-interval="20"></div>
                        </div>
                    </div>
                </div>
                
            </div>


            <div class="row clearfix">
                <!-- Task Info -->
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="card">
                        <div class="header bg-red">
                            <h2>Task list</h2>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-hover dashboard-task-infos" id="example">
                                    <thead>
                                        <tr>
                                            <th class="hidden"></th>
                                            <th>#</th>
                                            <th>Name</th>
                                            <th>IC</th>
                                            <th>Date Received</th>
                                            <th>Status</th>
                                            <th>Activity</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        <?php $i = 1; ?>

                                        @foreach($manager as $data)

                                            <?php 
                                                $date_rev =  date('Y-m-d H:i:s ', strtotime($data->created_at));
                                                $today    =  date('Y-m-d H:i:s');
                                                $to_time = strtotime($today);
                                                $from_time = strtotime($date_rev);
                                                $di= ($to_time - $from_time) / (60*60);
                                            ?> 

                                        <tr>
                                            <td class="hidden">{{number_format($di,0,'.','.')}}</td>
                                            <td>{{$i++}}</td>
                                            <td>{{$data->name}}</td>
                                            <td>{{$data->ic}}</td>

                                            <td>
                                                {{$data->created_at->toDayDateTimeString()}}
                                            </td>
                                            <td>
                                                <span class="label bg-{{$data->stages->color}}">{{$data->stages->desc}}</span>
                                            </td>
                                            
                                            
                                            <td class="text-center">
                                                <a href="{{url('/history/show/'.$data->id_cus)}}" class="btn bg-orange btn-circle waves-effect waves-circle waves-float" onclick="window.open('{{url('/history/show/'.$data->id_cus)}}', 'newwindow', 'width=750,height=400'); return false;"> <i class="material-icons">history</i> </a>
                                            </td>
                                            <td></td>
                                               
                                        </tr>
                                        @endforeach
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- #END# Task Info -->
                
            </div>





            @elseif($roless=='4') <!-- ===================== Task Processor 1 ==================== -->

            <div class="row clearfix">
                
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" onclick="location.href='{{route('assessment1.index')}}';" style="cursor:pointer; ">
                            <div class="info-box bg-red hover-expand-effect">
                                <div class="icon">
                                    <i class="material-icons">playlist_add_check</i>
                                </div>
                                
                                    <div class="content">
                                        <div class="text">New Application</div>
                                        <div class="number count-to" data-from="0" data-to="{{ $countw0 }}" data-speed="1000" data-fresh-interval="20"></div>
                                    </div>
                            </div>
                    </div>
                
                    
                
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" onclick="location.href='{{route('loan-eli.index')}}';">
                    <div class="info-box bg-light-green hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">file_upload</i>
                        </div>
                        <div class="content">
                            <div class="text">Uploaded Spekar</div>
                            <div class="number count-to" data-from="0" data-to={{$countp1upload_spekar}} data-speed="1000" data-fresh-interval="20"></div>
                        </div>
                    </div>
                </div>
                

            </div>

            <div class="row clearfix">
                
                <div class="card">
                    <div class="header bg-red">
                        <h2>Task List </h2>
                    </div>
                    
                   <div class="body">
                                <!-- <input  value="Generate" class="btn btn-success" > -->
                               <form method="POST" class="form-horizontal" id="popup-validation" action="{{ url('/send_aset')}}" >
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                <!-- <input  value="Generate" class="btn btn-success" > -->
                                <table class="table" id="example">
                                    <thead>
                                        <tr>
                                            
                                            <th>#</th>
                                            <th>By Hand</th>
                                            <th>Customer Name</th>
                                            <th>New IC</th>
                                            <!-- <th>Old IC (if Any)</th> -->
                                            <th>Company</th>
                                            <th>Doc</th>
                                            <th>Status</th>
                                            <th>Route Back</th>
                                            <th>MO</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $i = 1; ?>

                                        @foreach($processor1 as $data)

                                            <?php
                                               $datax1 = DB::table('doc_cust')->join('praapplication', 'doc_cust.cus_id', '=', 'praapplication.id_cus')->where('cus_id', $data->id_cus)->where('doc_cust.type','1')->latest('doc_cust.created_at')->limit('1')->first();

                                               $datax2 = DB::table('doc_cust')->join('praapplication', 'doc_cust.cus_id', '=', 'praapplication.id_cus')->where('cus_id', $data->id_cus)->where('doc_cust.type','2')->latest('doc_cust.created_at')->limit('1')->first();
                                            ?>
                                        <tr>
                                            <td>{{$i++}}</td>
                                            <td>{{ $data->created_at->toFormattedDateString() }}</td>
                                            <td>{{ $data->name }}</td>
                                            <td>{{ $data->ic }}</td>
                                            <td>{{ $data->majikan->Emp_Desc }}</td>
                                            <td>
                                                @if(empty($datax1->doc_pdf))
                                                    <span class="btn bg-red"><i class="material-icons disabled">do_not_disturb_alt</i> IC</span>
                                                @else
                                                    <a href="{{asset('/documents/user_doc/'.$data->ic.'/'.$datax1->doc_pdf)}}" target='_blank' class="btn bg-blue"><i class="material-icons">library_books</i> IC</a>
                                                @endif

                                                @if(empty($datax2->doc_pdf))
                                                    <span class="btn bg-red"><i class="material-icons disabled">do_not_disturb_alt</i>Consent Letter</span>
                                                @else
                                                    <a href="{{asset('/documents/user_doc/'.$data->ic.'/'.$datax2->doc_pdf)}}" target='_blank' class="btn bg-blue"><i class="material-icons">library_books</i> Consent Letter</a>
                                                @endif
                                            </td>
                                            <td>
                                                @include('shared.stage')
                                            </td>
                                            <td>
                                                @if(empty($datax1->doc_pdf) || empty($datax2->doc_pdf))
                                                    <button class="btn btn-sm btn-warning" type="button" data-toggle="modal" data-target="#defaultModal" style="margin-left: 10px"><i class="material-icons">add</i> Reject</button>
                                                @else
                                                    -
                                                @endif
                                            </td>
                                            <td>{{ $data->user->name}}</td>
                                            <td>
                                                @can('edit_assessments')
                                                <div class="demo-checkbox">
                                                    <input type="checkbox" id="basic_checkbox_{{$i}}" name="id[]" value="{{$data->id_cus}}" class="Bike" />

                                                    <label for="basic_checkbox_{{$i}}"></label>
                                                     <input type="hidden" id="id_cus" name="id_cus[]" value="{{$data->id_cus}}" />
                                                     <input type="hidden" id="ic" name="ic[]" value="{{$data->id_cus}}" />
                                                    <input type="hidden"  name="ci[]" value="{{$data->id_cus}}">
                                                    <input type="hidden"  name="ktp[]" value="{{$data->ic}}">

                                                     <input type="hidden"  name="ids[]" value="">
                                                     <input type="hidden"  name="sta[]" value="">
                                                </div>
                                                @endcan
                                            </td>

                                        </tr>
                                    @endforeach
                                    
                                    @can('edit_assessments')
                                    <button class="btn btn-sm btn-success" type="submit"><i class="material-icons">file_download</i> Generate & Send to MBSB</button>
                                    <!-- <a href="#" class="btn btn-sm btn-primary" data-target="#collapseDetailOne" data-toggle="collapse" style="margin-left: 20px"><i class="material-icons">send</i>Send MBSB</a>
                                        -->
                                    @endcan 

                                </form>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>  <!-- ===================== End Task Processor 1 ==================== -->

            
            @elseif($roless=='6')

                <div class="row clearfix">
                <!-- Task Info -->
                    <div class="card">
                        <div class="header bg-red">
                            <h2>Task List</h2>
                        </div>
                        <div class="body">

                        
                            <div class="table-responsive">
                                <table class="table table-hover dashboard-task-infos" id="example">
                                    <thead>
                                        <tr>
                                            <th width="10%">#</th>
                                            <th width="30%">Name</th>
                                            <th width="20%">IC</th>
                                            <th width="10%">Phone</th>
                                            <th width="10%">Status</th>
                                            <th width="5%">Detail</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        <?php $i = 1; ?>

                                        @foreach($process11 as $data)
                                        <tr>
                                            <td>{{$i++}}</td>
                                            <td>{{$data->name}}</td>
                                            <td>{{$data->ic}}</td>
                                            <td>{{$data->notelp}}</td> 
                                            <td>
                                                @include('shared.stage_new')
                                            </td>
                                            <td>
                                                <button class="btn btn-success" aria-controls="collapse-{{$data->id}}" data-target="#collapseDetailOne{{$data->id}}" data-toggle="collapse" style="cursor:pointer;">Detail</button>
                                            </td> 
                                            
                                        </tr>
                                        @endforeach

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
            </div>

            @endif

        </div>

        <!-- Gak jadi dulu -->
        <!-- modal detail -->
        @include('shared._modaldetail')
        <!-- end of modal detail -->


    </section>

    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>

    <script>
        $(document).ready(function() {
        $('#example').DataTable();
        });
    </script>

    <script type="text/javascript">
        
        $(document).ready(function() {
        $('#example2').DataTable();
        });
    </script>

@endsection