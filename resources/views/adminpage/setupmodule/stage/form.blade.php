
<div class="col-sm-12">
    <h5>Stage Code :</h5>
    <div class="form-group form-float @if ($errors->has('id_stage')) has-error @endif">
        <div class="form-line">            
            {!! Form::label('title', 'Stage Code', ['class' => 'form-label']) !!}
            {!! Form::text('id_stage', null, ['class' => 'form-control']) !!}
            @if ($errors->has('id_stage')) <p class="help-block">{{ $errors->first('id_stage') }}</p> @endif            
            <div class="help-info">Max 4 char</div>
        </div>
    </div>
</div>


<div class="col-sm-12">
    <h5>Description:</h5>
    <div class="form-group form-float @if ($errors->has('desc')) has-error @endif">
        <div class="form-line">            
            {!! Form::label('title', 'Description', ['class' => 'form-label']) !!}
            {!! Form::text('desc', null, ['class' => 'form-control']) !!}
            @if ($errors->has('desc')) <p class="help-block">{{ $errors->first('desc') }}</p> @endif            
        </div>
    </div>
</div>

<div class="col-sm-12">
    <h5>Color Stage:</h5>
    <div class="form-group form-float @if ($errors->has('color')) has-error @endif">
        <div class="form-line">            
            <select name="color" class="form-control">         
                <option value="red" style="color: #ffffff; background-color: #f44336" >Red</option>
                <option value="pink" style="color: #ffffff; background-color: #e91e63" >Pink</option>
                <option value="purple" style="color: #ffffff; background-color: #9c27b0" >Purple</option>
                <option value="deep-purple" style="color: #ffffff; background-color: #673ab7" >Deep Purple</option>
                <option value="indigo" style="color: #ffffff; background-color: #3f51b5">Indigo</option>
                <option value="blue" style="color: #ffffff; background-color: #2196f3" >Blue</option>
                <option value="light-blue" style="color: #ffffff ; background-color: #03a9f4" >Light Blue</option>
                <option value="light-green" style="color: #ffffff; background-color: #8bc34a" >Light Green</option>
                <option value="cyan" style="color: #ffffff; background-color: #009688" >Cyan</option>
                <option value="teal" style="color: #ffffff; background-color: #009688" >Teal</option>
                <option value="lime" style="color: #ffffff; background-color: #cddc39" >Lime</option>
                <option value="yellow" style="color: #ffffff; background-color: #ffe821" >Yellow</option>
                <option value="amber" style="color: #ffffff; background-color: #ffc107" >Amber</option>
                <option value="orange" style="color: #ffffff; background-color: #ff9800" >Orange</option>
                <option value="deep-orange" style="color: #ffffff; background-color: #ff5722">Deep Orange</option>
                <option value="brown" style="color: #ffffff; background-color: #795548" >Brown</option>
                <option value="grey" style="color: #ffffff; background-color: #9e9e9e" >Grey</option>
                <option value="blue-grey" style="color: #ffffff; background-color: #607d8b" >Blue Grey</option>
                <option value="black" style="color: #ffffff; background-color: #252525" >Black</option>
            </select>
            @if ($errors->has('color')) <p class="help-block">{{ $errors->first('color') }}</p> @endif            
        </div>
    </div>
</div>


<!-- {!! Form::text('color', null, ['class' => 'form-control']) !!} -->

