
    <div class="col-sm-12">
	    <h5>Nama Pakej:</h5>
	    <div class="form-group form-float @if ($errors->has('name')) has-error @endif">
	        <div class="form-line">
	            
	            {!! Form::label('title', 'Nama Pakej', ['class' => 'form-label']) !!}
	            {!! Form::text('name', null, ['class' => 'form-control']) !!}
	            @if ($errors->has('name')) <p class="help-block">{{ $errors->first('name') }}</p> @endif
	            
	        </div>
	    </div>
	</div>