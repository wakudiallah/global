<div class="col-sm-12">
    <h5>Employer Code:</h5>
    <div class="form-group form-float @if ($errors->has('Emp_Code')) has-error @endif">
        <div class="form-line">
            
            {!! Form::label('title', 'Employer Code', ['class' => 'form-label']) !!}
            {!! Form::text('Emp_Code', null, ['class' => 'form-control']) !!}
            @if ($errors->has('Emp_Code')) <p class="help-block">{{ $errors->first('Emp_Code') }}</p> @endif
            
        </div>
        <div class="help-info">Max 4 Char</div>
    </div>
</div>

<div class="col-sm-12">
    <h5>Employer Desc:</h5>
    <div class="form-group form-float @if ($errors->has('Emp_Desc')) has-error @endif">
        <div class="form-line">
            
            {!! Form::label('title', 'Employer Desc', ['class' => 'form-label']) !!}
            {!! Form::text('Emp_Desc', null, ['class' => 'form-control']) !!}
            @if ($errors->has('Emp_Desc')) <p class="help-block">{{ $errors->first('Emp_Desc') }}</p> @endif
            
        </div>
    </div>
</div>

<div class="col-sm-12">
    <h5>Status:</h5>
    <div class="form-group form-float @if ($errors->has('Act')) has-error @endif">
        <div class="form-line">
            
            {{Form::select('Act', ['1' => 'Active', '0' => 'Non Active'], '1', ['class' => 'form-control'])}}
            @if ($errors->has('Act')) <p class="help-block">{{ $errors->first('Act') }}</p> @endif
            
        </div>
    </div>
</div>

<div class="col-sm-12">
    <h5>Head:</h5>
    <div class="form-group form-float @if ($errors->has('head')) has-error @endif">
        <div class="form-line">
            
            {{Form::select('head', ['1' => 'Yes', '0' => 'No'], '1', ['class' => 'form-control'])}}
            @if ($errors->has('head')) <p class="help-block">{{ $errors->first('head') }}</p> @endif
            
        </div>
    </div>
</div>
