@extends('vadmin.tampilan')

@section('content')
    

    <section class="content">
        <div class="container-fluid">
            
            <div class="row clearfix"> <!-- Breadcrumber -->
                <div class="col-md-6">
                    <ol class="breadcrumb breadcrumb-col-pink">
                        <li><a href="javascript:void(0);"><i class="material-icons">text_fields</i> Attribute</a></li>
                        <li><a href="{{route('comm.index')}}"><i class="material-icons">money</i>Komisyen</a></li>
                        <li class="active"><i class="material-icons">create</i>Tambah</li>
                    </ol>
                </div>
            </div> <!-- End of breadcrumber -->

            <div class="row clearfix">
                <div class="col-md-12">
                    <div class="card">
                        <div class="header bg-red">
                            <h2>Tambah</h2>
                        </div>

                        <div class="body">
                            <div class="row clearfix">
                                {!! Form::open(['route' => ['comm.store'] ]) !!}
                                    
                                    @include('adminpage.setupmodule.comm.form')
                                    
                                    <div class="col-xs-6 col-sm-3 col-md-2 col-lg-2 col-md-offset-10">
                                        <button type="submit" class="btn bg-red btn-block btn-lg waves-effect">
                                            Simpan
                                        </button>
                                        {{ csrf_field() }}
                                    </div>
                                    
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>                
            </div>

        </div>
    </section>

    <!-- Jquery Core Js -->
    <script src="{{asset('admin/plugins/jquery/jquery.min.js')}}"></script>

    <!-- Custom Js -->
    <script src="{{asset('admin/js/pages/forms/basic-form-elements.js')}}"></script> 

    <script> 
        $.fn.datepicker.defaults.format = "dd-mm-yyyy";
    </script>

@endsection

