
<div class="col-sm-12">
    <h5>Kod Remark:</h5>
    <div class="form-group form-float @if ($errors->has('id_remark')) has-error @endif">
        <div class="form-line">            
            {!! Form::label('title', 'Kod Remark', ['class' => 'form-label']) !!}
            {!! Form::text('id_remark', null, ['class' => 'form-control']) !!}
            @if ($errors->has('id_remark')) <p class="help-block">{{ $errors->first('id_remark') }}</p> @endif            
            <div class="help-info">Max 4 char</div>
        </div>
    </div>
</div>


<div class="col-sm-12">
    <h5>Deskripsi Remark:</h5>
    <div class="form-group form-float @if ($errors->has('desc')) has-error @endif">
        <div class="form-line">            
            {!! Form::label('title', 'Deskripsi Remark', ['class' => 'form-label']) !!}
            {!! Form::text('desc', null, ['class' => 'form-control']) !!}
            @if ($errors->has('desc')) <p class="help-block">{{ $errors->first('desc') }}</p> @endif            
        </div>
    </div>
</div>

<div class="col-sm-12">
    <h5>Color Remark:</h5>
    <div class="form-group form-float @if ($errors->has('color')) has-error @endif">
        <div class="form-line">            
            {!! Form::label('title', 'Color Remark', ['class' => 'form-label']) !!}
            {!! Form::text('color', null, ['class' => 'form-control']) !!}
            @if ($errors->has('color')) <p class="help-block">{{ $errors->first('color') }}</p> @endif            
        </div>
    </div>
</div>

<!-- Roles Form Input -->
<div class="col-sm-12">
<div class="form-group form-float @if ($errors->has('roles')) has-error @endif">
    <div class="form-line">
        {!! Form::label('role_id', 'Pengguna') !!}
        {!! Form::select('role_id', $roles, isset($user) ? $user->roles->pluck('id')->toArray() : null,  ['class' => 'form-control']) !!}
    </div>
    @if ($errors->has('roles')) <p class="help-block">{{ $errors->first('roles') }}</p> @endif
</div>
</div>