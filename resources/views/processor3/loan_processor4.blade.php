@extends('vadmin.tampilan')

@section('content')

    <section class="content">
        <div class="container-fluid">

            <div class="row clearfix"> <!-- Breadcrumber -->
                <div class="col-md-6">
                    <ol class="breadcrumb breadcrumb-col-pink">
                        <li><a href="javascript:void(0);"><i class="material-icons">layers</i> Application Disbursement</a></li>
                    </ol>
                </div>
            </div> <!-- End of breadcrumber -->


            <!-- Notification -->
            <div class="row clearfix">
                @if ($message = Session::get('success')) 
                <div class="alert bg-green alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>{{ $message }}
                </div>
                @elseif($message = Session::get('delete'))
                <div class="alert bg-pink alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>{{ $message }}
                </div>
                @elseif($message = Session::get('update'))
                <div class="alert alert-warning alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>{{ $message }}
                </div>
                @endif
            </div>
            <!-- End of Notif -->


            <div class="row clearfix">
                <!-- Task Info -->
                    <div class="card">
                        <div class="header bg-red">
                            <h2>Application Disbursement</h2>
                        </div>
                        <div class="body">

                        
                            <div class="table-responsive">
                                <table class="table table-hover dashboard-task-infos" id="example">
                                    <thead>
                                        <tr>
                                            <th width="10%">#</th>
                                            <th width="30%">Name</th>
                                            <th width="20%">IC</th>
                                            <th width="10%">Phone</th>
                                            <th width="10%">Status</th>
                                            <th width="5%">Detail</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        <?php $i = 1; ?>

                                        @foreach($process11 as $data)
                                        <tr>
                                            <td>{{$i++}}</td>
                                            <td>{{$data->name}}</td>
                                            <td>{{$data->ic}}</td>
                                            <td>{{$data->notelp}}</td> 
                                            <td>
                                                @include('shared.stage_new')
                                            </td>
                                            <td>
                                                <button class="btn btn-success" aria-controls="collapse-{{$data->id}}" data-target="#collapseDetailOne{{$data->id}}" data-toggle="collapse" style="cursor:pointer;">Detail</button>
                                            </td> 
                                            
                                        </tr>
                                        @endforeach

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
            </div>


            <!-- ///////////////////  Data Target  ///////////////// -->
            @foreach($process11 as $datas)
            <div id="collapseDetailOne{{$datas->id}}" class="collapse" aria-expanded="false" data-collapse-group="collapse-group" aria-labelledby="headingFive" data-parent="#accordionExample">
                <div class="row clearfix">
                    <div class="card">
                        <div class="header bg-red">
                            <h2>Detail {{$datas->name}} </h2>
                        </div>
                        <div class="body">
                            
                            <div class="row">
                                <div class="col-md-6 "> <!-- kolom 1 -->
                                    <div class="row">
                                        <div class="col-md-6">
                                            <b>Name  </b>    
                                        </div>
                                        <div class="col-md-6">
                                           <input  class="form-control" type="text" name="" value="{{$datas->name}} " readonly> 
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <b>No Telf : </b>    
                                        </div>
                                        <div class="col-md-6">
                                            <input  class="form-control" type="text" name="" value="{{$datas->notelp}} " readonly>
                                           
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <b>Employer : </b>    
                                        </div>
                                        <div class="col-md-6">
                                            <input  class="form-control" type="text" name="" value="{{$datas->majikan->Emp_Desc}} " readonly>
                                           
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <b>By : </b>    
                                        </div>
                                        <div class="col-md-6">
                                           <p style="color: red">{{$datas->user->name}} </p>  
                                        </div>
                                    </div>   
                                </div> <!-- End of kolom 1 -->
                            </div>

                            <div class="row">
                                @if($datas->stage == 'W11' && $datas->stage == 'W12')
                                <a href="{{url('/appdisbursement/'.$datas->id_cus)}}" class="btn btn-sm btn-success" style="margin-left: 20px"><i class="material-icons">done_all</i> Approval</a>
                            
                                <a href="{{url('/rejectdisbursement/'.$datas->id_cus)}}" class="btn btn-sm btn-warning" style="margin-left: 10px"><i class="material-icons">not_interested</i> Reject</a>

                                <button class="btn btn-sm btn-primary" type="button" data-toggle="modal" data-target="#defaultModal" style="margin-left: 10px"><i class="material-icons">add</i> Additional Doc</button>
                                @else
                                @endif
                            </div>


                        </div>

                    </div>
                </div>
            </div>

            @endforeach
            <!-- ///////////////////  End Data Target  //////////////// -->



            <!-- ////////////////  Modal Route ///////////////-->
    @foreach($process11 as $dataz)
        <div class="modal fade" id="defaultModal" tabindex="-1" role="dialog">
                <div class="modal-dialog " role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="defaultModalLabel">Add Doc</h4>
                        </div>
                        <div class="modal-body">
                           {!! Form::open(array('url'=>'adddocprocess11/'.$dataz->id_cus, 'method'=>'post', 'files'=>'true')) !!}

                            {{ csrf_field() }} 

                            <input type="hidden" name="process5" value="{{$datas->process5}}">

                            <div class="row">
                                <div class="col-md-3">
                                    <b>Note :</b>
                                </div>
                                <div class="col-md-9">
                                    ​<textarea id="txtArea" name="note" class="form-control" rows="3"></textarea>
                                </div>
                            </div>

                            

                        </div>
                        <div class="modal-footer">
                            
                            <input type="submit" value="Submit" class="btn btn-lg btn-success">
                            <button type="button" class="btn btn-danger btn-lg waves-effect" data-dismiss="modal">Close</button>
                        </div>

                        {{ Form::close() }}   
                    </div>
                </div>
            </div> 
        @endforeach
    <!-- ////////////////  Modal Route ///////////////-->


        </div>
    </section>


    <!-- ================= Data Target hidden ======= -->
    <script src="https://code.jquery.com/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
     

    <script type="text/javascript">
        $("[data-collapse-group]").on('show.bs.collapse', function () {
              var $this = $(this);
              var thisCollapseAttr = $this.attr('data-collapse-group');
              $("[data-collapse-group='" + thisCollapseAttr + "']").not($this).collapse('hide');
            });
    </script>

    <!-- ================= End Data Target hidden ======= -->


    <script>
        $(document).ready(function() {
        $('#example').DataTable();
        });
    </script>

@endsection
