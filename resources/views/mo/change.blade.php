 @extends('vadmin.tampilan')


@section('content')
	<section class="content">
        <div class="container-fluid">

			<div class="row clearfix">
	    <!-- Task Info -->
	  
	 
	        <div class="card">
	            <div class="header bg-red">
	                <h2>Document Incomplete</h2>
	            </div>
	            <div class="body">
	                <div class="table-responsive">
	                    <table class="table table-hover dashboard-task-infos" id="example">
	                        <thead>
	                            <tr>
	                                <th width="5%">#</th>
	                                <th width="30%">Name</th>
	                                <th width="20%">IC</th>
	                                <th>Status</th> <!-- ketika dhantar submit kira berubah loan pending documentation -->
	                                <th>Note</th>
	                            </tr>
	                        </thead>
	                        <tbody>
	                            <?php $i = 1; ?>

	                            @foreach($change as $data)
	                            <tr>
	                                <td>{{$i++}}</td>
	                                <td>{{$data->name}}</td>
	                                <td>{{$data->ic}}</td>
	                                
	                                <!-- <td>
	                                     <button class="btn btn-success" aria-controls="collapse-{{$data->id}}" data-target="#collapseDetailOne{{$data->id}}" data-toggle="collapse" style="cursor:pointer;">Detail</button>
	                                </td>-->
	                                <td>
	                                     @include('shared.stage_new')
	                                </td> 

	                                <?php $note = DB::table('history')->where('cus_id', $data->id_cus)->latest('created_at')->limit('1')->first(); ?>

	                                <td>{{$note->note}}</td>
	                            </tr>

	                            @endforeach
	                            
	                        </tbody>
	                    </table>
	                </div>
	            </div>
	        </div>
	    
	    <!-- #END# Task Info -->
			</div>

			

			<!-- ///////////////////  Data Target  //////////////// -->
		    @foreach($change as $datas)
		    <div id="collapseDetailOne{{$datas->id}}" class="collapse" aria-expanded="false" data-collapse-group="collapse-group" aria-labelledby="headingFive" data-parent="#accordionExample">
		        <div class="row clearfix">
		            <div class="card">
		                <div class="header bg-red">
		                    <h2>Detail {{$datas->name}}</h2>
		                </div>
		                <div class="body">
		                   
		                   	 

		                </div>
		            </div>
		        </div>
		    </div>
		    @endforeach
		    <!-- ///////////////////  End Data Target  //////////////// -->

		</div>
	</section>


	<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>


	<script type="text/javascript">
		$(document).ready(function() {
	        $('#example').DataTable();
	        });
	</script>
	
@endsection

    