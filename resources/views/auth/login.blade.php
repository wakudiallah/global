@extends('layouts.main')

@section('content')
    <section class="probootstrap-cover overflow-hidden relative"  style="background-image: url('assets/images/kl.jpg');" data-stellar-background-ratio="0.5"  id="section-home">
    <div class="overlay"></div>
    <div class="container">
      <div class="row align-items-center">
        <div class="col-md">
          <h2 class="heading mb-2 display-4 font-light probootstrap-animate">Global I Exceed Management Sdn.Bhd</h2> 
          <p class="lead mb-5 probootstrap-animate">
          <!-- </p>
            <a href="onepage.html" role="button" class="btn btn-primary p-3 mr-3 pl-5 pr-5 text-uppercase d-lg-inline d-md-inline d-sm-block d-block mb-3">See OnePage Verion</a> 
          </p> -->
        </div> 
        <div class="col-md probootstrap-animate">
          <form action="{{ route('login') }}" method="POST" class="probootstrap-form border border-danger">
            {{ csrf_field() }}
               <span id="latitude"></span>
                            <span id="longitude"></span>
                            <span id="location"></span>
            <div class="form-group">
              <div class="col-md">
                   <div class="panel panel-default">

                      <div class="panel-body">
                          <div class="form-group">
                              <label for="email" class="col-md-4 control-label">E-Mail Address</label>
                              <div class="probootstrap-date-wrap">
                                  <span class="icon ion-card"></span>
                                  <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>

                                  @if ($errors->has('email'))
                                      <span class="help-block">
                                          <strong>{{ $errors->first('email') }}</strong>
                                      </span>
                                  @endif
                              </div>
                          </div>

                          <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                              <label for="probootstrap-date-departure" class="col-md-12 control-label">Password</label>

                              <div class="probootstrap-date-wrap">
                                  <span class="icon ion-key"></span>
                                  <input id="password" type="password" class="form-control" name="password" required>

                                  @if ($errors->has('password'))
                                      <span class="help-block">
                                          <strong>{{ $errors->first('password') }}</strong>
                                      </span>
                                  @endif
                              </div>
                          </div>

                          <div class="form-group">
                              <div class="col-md-6 col-md-offset-4">
                                  <div class="checkbox">
                                      <label>
                                          <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                                      </label>
                                  </div>
                              </div>
                          </div>

                          <div class="form-group">
                              <div class="row">
                                <div class="col-md"></div>
                                <div class="col-md">
                                  <button type="submit" class="btn btn-danger btn-block" style="cursor:pointer;">
                                      Login
                                  </button>
                                </div>
                              </div>
                              <div class="col-md-12 link-text">
                                  <a href="{{ route('password.request') }}" onmouseover="this.style.color='red';" onmouseout="this.style.color='black';">
                                      Lupa kata laluan?
                                  </a>
                              </div>
                          </div>
                      </div>
                  </div> 
                </div>   
            </div>
          </form>
        </div>



      </div>
    </div>
  </section>
<input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">

<script>
var apiGeolocationSuccess = function(position) {
  showLocation(position);
};

var tryAPIGeolocation = function() {
  jQuery.post( "https://www.googleapis.com/geolocation/v1/geolocate?key=AIzaSyDCa1LUe1vOczX1hO_iGYgyo8p_jYuGOPU", function(success) {
    apiGeolocationSuccess({coords: {latitude: success.location.lat, longitude: success.location.lng}});
  })
  .fail(function(err) {
    // alert("API Geolocation error! \n\n"+err);
    window.location.href = "/geolocation/error/"+err;
  });
};

var browserGeolocationSuccess = function(position) {
  showLocation(position);
};

var browserGeolocationFail = function(error) {
  switch (error.code) {
    case error.TIMEOUT:
      alert("Browser geolocation error !\n\nTimeout.");
      break;
    case error.PERMISSION_DENIED:
      if(error.message.indexOf("Only secure origins are allowed") == 0) {
        tryAPIGeolocation();
      }
         window.location.href = "{{url('/')}}/geolocation/error/"+error.code;
      break;
    case error.POSITION_UNAVAILABLE:
      alert("Browser geolocation error !\n\nPosition unavailable.");
      break;
  }
};

var tryGeolocation = function() {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(
      browserGeolocationSuccess,
      browserGeolocationFail,
      {maximumAge: 50000, timeout: 20000, enableHighAccuracy: true});
  }
};

tryGeolocation();

function showLocation(position) {
  var latitude = position.coords.latitude;
  var longitude = position.coords.longitude;
  var _token = $('#_token').val();
  $.ajax({
    type:'POST',
    url: "{{url('/')}}/getLocation",
    data: { latitude: latitude, _token : _token, longitude : longitude },
    success:function(data){
            if(data){
              $("#latitude").html("<input type='hidden' name='latitude' id='latitude' value='"+data.latitude+"'>");
              $("#longitude").html("<input type='hidden' name='longitude' id='longitude' value='"+data.longitude+"'>");
               $("#location").html("<input type='hidden' name='location' id='location' value='"+data.location+"'>");
                 

            }else{
                $("#location").html('Not Available');
            }
    }
  });
}
</script>  
@endsection
