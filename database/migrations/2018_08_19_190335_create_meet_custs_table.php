<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMeetCustsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('meet_cust'))
        {
            Schema::create('meet_cust', function (Blueprint $table) {
                $table->increments('id');
                $table->String('cus_id',100)->nullable();
                $table->boolean('clarical')->default(1);
                $table->boolean('existing')->default(1);
                $table->date('date_disbustment');
                $table->SoftDeletes();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('meet_cust');
    }
}
