<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRemarksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('remark'))
        {
            Schema::create('remark', function (Blueprint $table) {
                $table->increments('id');
                $table->String('id_remark',4);
                $table->String('desc',60);
                $table->String('color',60);
                $table->String('color',60);
                $table->integer('role_id')->nullable();

                $table->unsignedInteger('user_id');
                $table->foreign('user_id')
                    ->references('id')->on('users')
                    ->onDelete('cascade');

                $table->SoftDeletes();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('remark');
    }
}
