<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void `ID1`, ``, ``, ``, `Comm_MO`, `Comm_TeamLead`, `Comm_Manager`, `Usr_ID`, `Usr_Name`, `UsrGrp_Code`, `Act`, `Last_UpDt``ID1`, `LnPkg_Code`, `Dt_Start`, `Dt_Ent`, `Comm_MO`, `Comm_TeamLead`, `Comm_Manager`, `Usr_ID`, `Usr_Name`, `UsrGrp_Code`, `Act`, `Last_UpDt`
     */
    public function up()
    {
        if(!Schema::hasTable('comm'))
        {
            Schema::create('comm', function (Blueprint $table) {
                $table->increments('id');
                $table->String('LnPkg_Code',4);
                $table->date('Dt_Start')->nullable();
                $table->date('Dt_Ent')->nullable();
                $table->double('Comm_MO', 13, 2)->nullable();
                $table->double('Comm_TeamLEad', 13, 2)->nullable();    
                $table->double('Comm_Manager', 13, 2)->nullable();
                $table->boolean('Act')->defult(1);
                $table->unsignedInteger('user_id');

                $table->foreign('user_id')
                    ->references('id')->on('users')
                    ->onDelete('cascade');
                    
                $table->SoftDeletes();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comms');
    }
}
