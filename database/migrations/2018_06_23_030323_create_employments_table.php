<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmploymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('employment'))
        {
            Schema::create('employment', function (Blueprint $table) {
                $table->increments('id');
                $table->string('code_employment');
                $table->String('name',90);
                $table->unsignedInteger('user_id');

                $table->foreign('user_id')
                    ->references('id')->on('users')
                    ->onDelete('cascade');
                    
                $table->SoftDeletes();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employments');
    }
}
