<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocAssestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('doc_assest'))
        {
            Schema::create('doc_assest', function (Blueprint $table) {
                $table->increments('id');
                $table->String('file',2000)->nullable();
                $table->integer('status')->unsigned()->nullable();
                $table->integer('user_id')->unsigned()->nullable();
                $table->SoftDeletes();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('doc_assest');
    }
}
