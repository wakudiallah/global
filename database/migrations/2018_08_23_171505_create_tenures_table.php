<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTenuresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('tenure'))
        {
            Schema::create('tenure', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('years')->unsigned()->nullable();
                $table->double('rate')->nullable();
                $table->integer('id_loan')->unsigned()->nullable();
                $table->SoftDeletes();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tenure');
    }
}
