<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('cal'))
        {
            Schema::create('cal', function (Blueprint $table) {
                $table->increments('id');
                $table->date('dt_start')->nullable();
                $table->date('dt_stop')->nullable();
                $table->String('desc', 150)->nullable();
                $table->boolean('act')->defult(true);
                $table->unsignedInteger('user_id');

                $table->foreign('user_id')
                        ->references('id')->on('users')
                        ->onDelete('cascade');
                $table->SoftDeletes();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cal');
    }
}
