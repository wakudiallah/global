<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocCustsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('doc_cust'))
        {
            Schema::create('doc_cust', function (Blueprint $table) {
                $table->increments('id');
                $table->string('cus_id',40);
                $table->string('doc_pdf',250)->nullable();
                $table->string('name',40)->nullable();
                $table->integer('type')->unsigned();
                $table->integer('verification')->unsigned()->nullable();
                $table->Integer('user_id')->unsigned()->nullable();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('doc_cust');
    }
}
