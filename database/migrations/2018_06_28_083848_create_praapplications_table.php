<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePraapplicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('praapplication'))
        {
            Schema::create('praapplication', function (Blueprint $table) {
                $table->increments('id');
                $table->String('id_cus',40);
                $table->String('name');
                $table->string('ic');
                $table->string('notelp',255);

                $table->string('employment_code',4)->nullable();
                $table->foreign('employment_code')
                    ->references('code_employment')->on('employment')
                    ->onDelete('cascade');

                $table->string('emp_code',4);                
                $table->foreign('emp_code')
                    ->references('Emp_Code')->on('emp')
                    ->onDelete('cascade');

                $table->double('elaun', 13, 2)->nullable();
                $table->double('pot_bul', 13, 2)->nullable();
                $table->string('loanpkg_code', 4);
                $table->foreign('loanpkg_code')
                    ->references('LnPkg_Code')->on('loanpkg')
                    ->onDelete('cascade');

                $table->double('gaji_asas', 13, 2)->nullable();
                $table->double('jml_pem', 13, 2)->nullable();
                
                $table->double('latitude', 13, 2);
                $table->double('longitude', 13, 2);
                $table->SoftDeletes();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('praapplication');
    }
}
